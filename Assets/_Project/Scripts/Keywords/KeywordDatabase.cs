﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using TorkFramework.Serialization;
using UnityEngine;
using UnityEngine.Profiling;

namespace Kairos.Keywords
{
    public static class KeywordDatabase
    {
        private static Dictionary<string, KeywordInfo> m_LoadedKeywords;

        public static Dictionary<string, KeywordInfo> LoadedKeywords
        {
            get
            {
                if (m_LoadedKeywords == null)
                    Load();
                return m_LoadedKeywords;
            }
            private set => m_LoadedKeywords = value;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Init()
        {
            Load();
        }

        public static void Load()
        {
            m_LoadedKeywords = new Dictionary<string, KeywordInfo>();
            
            if (!File.Exists(Globals._KEYWORDS_PATH))
            {
                Save();
                return;
            }
            
            var deserializer = new Deserializer();
            var result = deserializer.DeserializeFromFile<List<KeywordInfo>>(Globals._KEYWORDS_PATH);
            if (result != null)
            {
                foreach (var keyword in result)
                {
                    LoadedKeywords.Add(keyword.KeywordText.ToUpper(), keyword);
                }
            }
        }

        public static void Save()
        {
            if (!Directory.Exists(Path.GetDirectoryName(Globals._KEYWORDS_PATH)))
                Directory.CreateDirectory(Path.GetDirectoryName(Globals._KEYWORDS_PATH));
            
            var valueToSerialize = new List<KeywordInfo>();
            foreach (var loadedKeyword in LoadedKeywords)
            {
                valueToSerialize.Add(loadedKeyword.Value);
            }
            
            var serializer = new Serializer(new XmlWriterSettings());
            serializer.SerializeToFile(Globals._KEYWORDS_PATH, valueToSerialize);
        }

        public static string GetFormattedText(string text)
        {
            string resultString = "";
            bool found = false;
            for (int i = text.Length - 1; i >= 0; i--)
            {
                if (!found)
                {
                    if (text[i] != '\n' && text[i] != '\r')
                    {
                        found = true;
                    }
                }
                
                if (found)
                {
                    resultString = text[i] + resultString;
                }
            }
            
            return resultString;
            var builder = new StringBuilder();
            foreach (var splitDot in text.Split('.'))
            {
                foreach (var splitReturn in splitDot.Replace("\r", "\n").Split('\n'))
                {
                    for (var i = 0; i < splitReturn.Split(' ').Length; i++)
                    {
                        var word = splitReturn.Split(' ')[i];
                        if (LoadedKeywords.ContainsKey(word.ToUpper()))
                        {
                            var targetKeyword = LoadedKeywords[word.ToUpper()];
                            builder.Append(targetKeyword.KeywordStyleOpener);
                            builder.Append(word);
                            for (int j = 0; j < targetKeyword.NextWordsToInclude; j++)
                            {
                                i++;
                                builder.Append(" ");
                                builder.Append(splitReturn.Split(' ')[i]);
                            }
                            builder.Append(targetKeyword.KeywordStyleCloser);
                        }
                        else
                        {
                            builder.Append(word);
                        }
                        
                        if(Array.IndexOf(splitReturn.Split(' '), word) < splitReturn.Split(' ').Length - 1)
                            builder.Append(" ");
                    }
                    
                    if (Array.IndexOf(splitDot.Split('\n'), splitReturn) < splitDot.Split('\n').Length - 1)
                    {
                        builder.Append('.');
                        builder.Append('\n');
                    }
                }
            }

            return builder.ToString();
        }

        public static List<string> GetKeywordsInText(string text)
        {
            var result = new List<string>();
            foreach (var word in text.Replace("\r", "\n").Replace("\n", " ").Replace(".", " ").Split(' '))
            {
                var upperWord = word.ToUpper();
                if (LoadedKeywords.ContainsKey(upperWord) && !result.Contains(upperWord))
                {
                    result.Add(upperWord);
                }
            }

            return result;
        }

        public static string[] GetKeywordsList()
        {
            string[] result = new string[LoadedKeywords.Count];
            var counter = 0;
            foreach (var key in LoadedKeywords.Keys)
            {
                result[counter] = key;
                counter++;
            }

            return result;
        }
    }
}