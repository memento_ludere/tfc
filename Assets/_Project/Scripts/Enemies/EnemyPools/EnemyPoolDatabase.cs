﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Kairos.Enemies.EnemyPools
{
    public static class EnemyPoolDatabase
    {
        private static Dictionary<string, EnemyPoolInfo> m_LoadedEnemyPools;

        public static Dictionary<string, EnemyPoolInfo> LoadedEnemyPools
        {
            get
            {
                if (m_LoadedEnemyPools == null)
                    Reload();
                return m_LoadedEnemyPools;
            }
        }

        public static EnemyPoolInfo[] GetEnemyPoolList()
        {
            var result = new EnemyPoolInfo[LoadedEnemyPools.Count];

            int counter = 0;
            foreach (var loadedEnemyPool in LoadedEnemyPools)
            {
                result[counter] = loadedEnemyPool.Value;
                counter++;
            }

            return result;
        }

        public static string[] GetIdList()
        {
            var result = new string[LoadedEnemyPools.Count];

            int counter = 0;
            foreach (var loadedPool in LoadedEnemyPools)
            {
                result[counter] = loadedPool.Key;
                counter++;
            }

            return result;
        }
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        public static void Reload()
        {
            m_LoadedEnemyPools = new Dictionary<string, EnemyPoolInfo>();

            var filesToLoad = Directory.GetFiles(Globals._ENEMY_POOLS_FOLDER);
            foreach(var file in filesToLoad)
            {
                if(Path.GetExtension(file).ToLower() == ".enemypool")
                {
                    var newPool = EnemyPoolHelper.LoadEnemyPool(file);
                    LoadedEnemyPools.Add(newPool.PoolId, newPool);
                }
            }
        }
    }
}