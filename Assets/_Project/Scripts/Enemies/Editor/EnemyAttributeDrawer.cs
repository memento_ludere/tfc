﻿using System;
using System.Collections.Generic;
using Kairos.Units;
using UnityEditor;
using UnityEngine;

namespace Kairos.Enemies.Editor
{
    [CustomPropertyDrawer(typeof(EnemyAttribute))]
    public class EnemyAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var enemyIds = EnemyDatabase.GetIdList();

            var actualId = Array.IndexOf(enemyIds, property.stringValue);
            if(actualId < 0)
                actualId = enemyIds.Length;

            var enemyName = new List<string>();
            foreach (var enemyId in enemyIds)
            {
                try
                {
                    enemyName.Add(UnitDatabase.LoadedUnits[EnemyDatabase.LoadedEnemies[enemyId].Unit].UnitName);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex.Message);
                }
            }
            
            EditorGUI.LabelField(new Rect(position.x, position.y, 100, position.height), label);
            var newId = EditorGUI.Popup(new Rect(position.x + 100, position.y, position.width-100, position.height), actualId, enemyName.ToArray());

            if (newId != actualId)
            {
                property.stringValue = newId < enemyIds.Length ? enemyIds[newId] : "";
            }
        }

        public static string Draw(string label, string selectedId)
        {
            var enemyIds = EnemyDatabase.GetIdList();

            var actualId = Array.IndexOf(enemyIds, selectedId);
            if(actualId < 0)
                actualId = enemyIds.Length;

            var enemyName = new List<string>();
            foreach (var enemyId in enemyIds)
            {
                enemyName.Add(UnitDatabase.LoadedUnits[EnemyDatabase.LoadedEnemies[enemyId].Unit].UnitName);
            }
            
            EditorGUILayout.LabelField(label);
            var newId = EditorGUILayout.Popup(actualId, enemyName.ToArray());
            
            return newId < enemyIds.Length ? enemyIds[newId] : "";
        }
    }
}