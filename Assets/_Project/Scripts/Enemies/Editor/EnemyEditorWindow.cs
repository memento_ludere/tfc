﻿using System;
using System.IO;
using Kairos.Cards.Editor;
using Kairos.Enemies.AI;
using TorkFramework.Editor.Utility;
using TorkFramework.Utility;
using UnityEditor;
using UnityEngine;

namespace Kairos.Enemies.Editor
{
    public class EnemyEditorWindow : EditorWindow
    {
        private string m_SelectedEnemyPath;
        private Vector2 m_ScrollPosition;
        public EnemyInfo SelectedEnemy;
        
        [MenuItem("Custom/Enemies/Reload")]
        public static void ReloadEnemiesDatabase()
        {
            EnemyDatabase.Reload();
        }
        
        [MenuItem("Custom/Enemies/New")]
        public static void CreateNewEnemy()
        {
            var newEnemy = EnemyHelper.GenerateNewEnemy();
            EnemyHelper.SaveEnemy(newEnemy, Globals._ENEMIES_FOLDER + "/newEnemy.enemy");
            EnemyDatabase.Reload();
        }

        [MenuItem("Custom/Enemies/Editor")]
        public static void Init()
        {
            var window = GetWindow<EnemyEditorWindow>();
            window.name = "Enemy Editor";
            window.titleContent = new GUIContent("Enemy Editor");
            window.Show();
        }
        
        private void OnEnable()
        {
            OnSelectionChange();
        }
        
        private void OnSelectionChange()
        {
            if (Selection.objects.Length == 1 && Path.GetExtension(AssetDatabase.GetAssetPath(Selection.objects[0])).ToLower() == ".enemy")
            {
                m_SelectedEnemyPath = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + AssetDatabase.GetAssetPath(Selection.objects[0]);
                SelectedEnemy = EnemyHelper.LoadEnemy(m_SelectedEnemyPath);
            }

            Repaint();
        }
        
        private void OnGUI()
        {
            if (SelectedEnemy == null)
                return;

            m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.TextField("ID", SelectedEnemy.EnemyId);
            EditorGUI.EndDisabledGroup();
            
            var so = new SerializedObject(this);
            var soEnemy = so.FindProperty("SelectedEnemy");
            var soUnit = soEnemy.FindPropertyRelative("Unit");
            EditorGUILayout.PropertyField(soUnit);
            so.ApplyModifiedProperties();

            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Boxes", EditorStyles.boldLabel);
            for (var i = 0; i < SelectedEnemy.Brain.Boxes.Length; i++)
            {
                var box = SelectedEnemy.Brain.Boxes[i];
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Box " + i);
                var previousColor = GUI.color;
                GUI.color = Color.red;
                if (GUILayout.Button("X", GUILayout.ExpandWidth(false)))
                {
                    SelectedEnemy.Brain.RemoveBox(i);
                    Repaint();
                }
                GUI.color = previousColor;
                EditorGUILayout.EndHorizontal();
                EditorGUI.indentLevel++;

                box.LoopType = (EnemyAIBox.BoxLoopType) EditorGUILayout.EnumPopup("Loop Type", box.LoopType);
                EditorGUILayout.LabelField("Turn Sequence");
                EditorGUI.indentLevel++;
                var turnsCount = EditorGUILayout.IntField("Turns Count", box.TurnSequence.Length);
                if (turnsCount != box.TurnSequence.Length)
                {
                    box.Resize(turnsCount);
                    Repaint();
                }

                for (var turnIndex = 0; turnIndex < box.TurnSequence.Length; turnIndex++)
                {
                    var turn = box.TurnSequence[turnIndex];
                    EditorGUILayout.LabelField("Turn " + turnIndex, EditorStyles.boldLabel);
                    EditorGUI.indentLevel++;
                    var actionsCount = EditorGUILayout.IntField("Actions Count", turn.TurnCards.Length);
                    if (actionsCount != turn.TurnCards.Length)
                    {
                        turn.Resize(actionsCount);
                        Repaint();
                    }

                    for (var actionIndex = 0; actionIndex < turn.TurnCards.Length; actionIndex++)
                    {
                        EditorGUI.indentLevel++;
                        turn.TurnCards[actionIndex] = CardAttributeDrawer.CardField("Action " + actionIndex,turn.TurnCards[actionIndex]);
                        EditorGUI.indentLevel--;
                    }

                    EditorGUI.indentLevel--;
                }

                EditorGUI.indentLevel--;
                EditorGUI.indentLevel--;
            }

            var oldColor = GUI.color;
            GUI.color = Color.green;
            if (GUILayout.Button("Add Box"))
            {
                SelectedEnemy.Brain.AddBox();
                Repaint();
            }

            GUI.color = oldColor;

            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Transitions", EditorStyles.boldLabel);
            for (int i = 0; i < SelectedEnemy.Brain.Transitions.Length; i++)
            {
                var transition = SelectedEnemy.Brain.Transitions[i];
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Transition " + i);
                GUI.color = Color.red;
                if (GUILayout.Button("X", GUILayout.ExpandWidth(false)))
                {
                    SelectedEnemy.Brain.RemoveTransition(i);
                    Repaint();
                }
                GUI.color = oldColor;
                EditorGUILayout.EndHorizontal();
                
                EditorGUI.indentLevel++;
                var transitionDisplayOptions = new GUIContent[SelectedEnemy.Brain.Boxes.Length];
                var transitionOptions = new int[SelectedEnemy.Brain.Boxes.Length];
                for (int optionIndex = 0; optionIndex < transitionDisplayOptions.Length; optionIndex++)
                {
                    transitionDisplayOptions[optionIndex] = new GUIContent("Box " + optionIndex);
                    transitionOptions[optionIndex] = optionIndex;
                }

                transition.Source = EditorGUILayout.IntPopup(new GUIContent("Source"), transition.Source, transitionDisplayOptions, transitionOptions);
                transition.Target = EditorGUILayout.IntPopup(new GUIContent("Target"), transition.Target, transitionDisplayOptions, transitionOptions);
                transition.Priority = (int) FieldUtils.AdaptiveField("Priority", (dynamic) transition.Priority);
               
                if (transition.Condition != null)
                {
                    transition.Condition = FieldUtils.AdaptiveField("Condition -> " + transition.Condition.GetType().Name, (dynamic) transition.Condition) as EnemyAITransitionCondition;
                }
                else
                {
                    EditorGUILayout.LabelField("Condition -> None");
                }

                if (GUILayout.Button("Set Condition"))
                {
                    var menu = new GenericMenu();
                    foreach (var transtionType in TypeUtils.GetDerivedTypes(typeof(EnemyAITransitionCondition)))
                    {
                        var targetTransition = transition;
                        menu.AddItem(new GUIContent(transtionType.Name), false, () => {OnAddNewTransitionClicked(targetTransition, transtionType);} );
                    }
                    menu.ShowAsContext();
                }
                EditorGUI.indentLevel--;
            }

            GUI.color = Color.green;
            if (GUILayout.Button("Add Transition"))
            {
                SelectedEnemy.Brain.AddTransition();
                Repaint();
            }

            GUI.color = oldColor;

            if (GUILayout.Button("Save"))
            {
                EnemyHelper.SaveEnemy(SelectedEnemy, m_SelectedEnemyPath);
                Repaint();
            }
            

            EditorGUILayout.EndScrollView();
            
        }

        private void OnAddNewTransitionClicked(EnemyAITransition targetTransition, Type transitionType)
        {
            targetTransition.Condition = Activator.CreateInstance(transitionType) as EnemyAITransitionCondition;
        }
    }
}