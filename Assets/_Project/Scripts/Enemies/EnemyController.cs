﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kairos.Cards;
using Kairos.Enemies.AI;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework;
using UnityEngine;

namespace Kairos.Enemies
{
    [RequireComponent(typeof(UnitController))]
    public class EnemyController : EntityBehaviour<GameEntity>, ISelectable
    {
        [SerializeField, Enemy] private string m_ReferencedEnemy;
        [SerializeField] private float m_ActionDeclareDelay = 1f;
        [SerializeField] private Material m_HeroSelectedMaterial;
        [SerializeField] private Material m_EnemySelectedMaterial;
        private EnemyAIBrain m_Brain;
        private UnitController m_UnitController;
        private TimelineController m_Timeline;

        private MeshRenderer m_Renderer;
        private Material[] m_DefaultMaterials;

        public EnemyInfo ReferencedEnemy => EnemyDatabase.LoadedEnemies[m_ReferencedEnemy];

        public UnitController UnitController
        {
            get
            {
                if (m_UnitController == null)
                    m_UnitController = GetComponent<UnitController>();
                return m_UnitController;
            }
        }

        private void Awake()
        {
            m_Renderer = GetComponentInChildren<MeshRenderer>();
            m_DefaultMaterials = new Material[m_Renderer.materials.Length];
            for (int i = 0; i < m_Renderer.materials.Length; i++)
            {
                m_DefaultMaterials = m_Renderer.materials;
            }
        }

        public void Load(TimelineController timeline)
        {
            m_Timeline = timeline;
            m_Brain = ReferencedEnemy.GetBrainInstance();
        }

        public void DeclareActions(Action actionsDeclaredCallback)
        {
            if (!UnitController.IsDead)
            {
                var targetTurnAction = m_Brain.Tick(this);
                var actionsList = targetTurnAction.TurnCards.ToList();

                m_Timeline.AssignCard(false, CardDatabase.LoadedCards[actionsList[0]], UnitController,
                    (result, action) => { Entity.GameManager.GetManager<TimeManager>().WaitTime(m_ActionDeclareDelay, () => { OnCardAssigned(1, actionsList, actionsDeclaredCallback); }); });
            }
        }

        private void OnCardAssigned(int actualIndex, List<string> cardsToAssign, Action actionsDeclaredCallback)
        {
            if (actualIndex >= cardsToAssign.Count)
                actionsDeclaredCallback?.Invoke();
            else
                m_Timeline.AssignCard(false, CardDatabase.LoadedCards[cardsToAssign[actualIndex]], UnitController,
                    (result, action) => { Entity.GameManager.GetManager<TimeManager>().WaitTime(m_ActionDeclareDelay, () => { OnCardAssigned(actualIndex + 1, cardsToAssign, actionsDeclaredCallback); }); });
        }

        public void OnSelect(Team selectionTeam)
        {
            var materials = new Material[m_Renderer.materials.Length];
            for (var i = 0; i < m_Renderer.materials.Length; i++)
            {
                if (selectionTeam == Team.Player)
                    materials[i] = new Material(m_HeroSelectedMaterial);
                else
                    materials[i] = new Material(m_EnemySelectedMaterial);
            }

            m_Renderer.materials = materials;
        }

        public void OnDeselect()
        {
            m_Renderer.materials = m_DefaultMaterials;
        }
    }
}