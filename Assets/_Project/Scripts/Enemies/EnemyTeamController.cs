﻿using System;
using System.Collections.Generic;
using Kairos.Controllers;
using Kairos.Enemies.EnemyPools;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework;

namespace Kairos.Enemies
{
    public class EnemyTeamController : EntityBehaviour<GameEntity>
    {
        private BoardController m_Board;
        private TimelineController m_Timeline;
        private List<EnemyController> m_Enemies;

        public void Load(BoardController board, TimelineController timeline,EnemyPoolReference enemies, Action controllerLoadedCallback)
        {
            m_Board = board;
            m_Timeline = timeline;
            SpawnEnemies(enemies, controllerLoadedCallback);
        }
        
        public void Unload()
        {
            
        }

        public void OnTurnStarted(Action turnStartedCallback)
        {
            DeclareActions(turnStartedCallback);
        }
        
        public void SpawnEnemies(EnemyPoolReference enemies, Action enemiesSpawnedCallback)
        {
             m_Enemies = new List<EnemyController>();

            EnemyPoolInfo selectedEnemyPool = EnemyPoolDatabase.LoadedEnemyPools[enemies.PoolId];

            m_Board.SpawnEnemy(new UnitData(UnitDatabase.LoadedUnits[EnemyDatabase.LoadedEnemies[selectedEnemyPool.Enemies[0]].Unit]), controller =>
            {
                m_Enemies.Add(controller.GetComponent<EnemyController>());
                m_Enemies[m_Enemies.Count-1].Load(m_Timeline);
                OnEnemySpawned(0, selectedEnemyPool, m_Board, m_Timeline, enemiesSpawnedCallback);
            });
        }

        private void OnEnemySpawned(int actualIndex, EnemyPoolInfo enemyPool, BoardController board, TimelineController timeline, Action enemiesSpawnedCallback)
        {
            actualIndex++;
            if (actualIndex >= enemyPool.Enemies.Length)
            {
                enemiesSpawnedCallback?.Invoke();
            }
            else
            {
                board.SpawnEnemy(new UnitData(UnitDatabase.LoadedUnits[EnemyDatabase.LoadedEnemies[enemyPool.Enemies[actualIndex]].Unit]),controller =>
                {
                    m_Enemies.Add(controller.GetComponent<EnemyController>());
                    m_Enemies[m_Enemies.Count-1].Load(m_Timeline);
                    OnEnemySpawned(actualIndex, enemyPool, board, timeline, enemiesSpawnedCallback);
                });
            }
        }
        
        private void DeclareActions(Action actionsDeclareCallback)
        {
            DeclareActionsRecursive(0, actionsDeclareCallback);
        }

        private void DeclareActionsRecursive(int actualIndex, Action actionsDeclareCallback)
        {
            if (actualIndex >= m_Enemies.Count)
            {
                actionsDeclareCallback?.Invoke();
            }
            else
            {
                if (m_Enemies[actualIndex].UnitController.IsDead)
                {
                    DeclareActionsRecursive(actualIndex + 1, actionsDeclareCallback);
                }
                else
                {
                    m_Enemies[actualIndex].DeclareActions(() => { DeclareActionsRecursive(actualIndex + 1, actionsDeclareCallback); });
                }
            }
        } 
    }
}