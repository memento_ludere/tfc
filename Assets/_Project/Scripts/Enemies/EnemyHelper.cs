﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using TorkFramework.Serialization;
using UnityEditor;

namespace Kairos.Enemies
{
    public class EnemyHelper
    {
        public static void SaveEnemy(EnemyInfo enemy, string path)
        {
#if UNITY_EDITOR
            var serializer = new Serializer(new XmlWriterSettings());
            serializer.SerializeToFile(path, enemy);

            AssetDatabase.Refresh();
#endif
        }
        
        public static EnemyInfo LoadEnemy(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();

            if (Path.GetExtension(path).ToLower() != ".enemy")
                throw new BadImageFormatException();

            var deserializer = new Deserializer();
            return deserializer.DeserializeFromFile<EnemyInfo>(path);
        }
        
        private static string GenerateEnemyID()
        {
            var result = default(byte[]);

            using (var stream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
                {
                    writer.Write(DateTime.Now.Ticks);
                }

                stream.Position = 0;

                using (var hash = SHA256.Create())
                {
                    result = hash.ComputeHash(stream);
                }
            }

            var resultText = "";
            for (int i = 0; i < result.Length; i++)
            {
                resultText += result[i].ToString("x2");
            }

            return "enemy-" + resultText;    
        }
        
        public static EnemyInfo GenerateNewEnemy()
        {
            return new EnemyInfo {EnemyId = GenerateEnemyID()};
        }
    }
}