﻿namespace Kairos.Enemies.AI
{
    public abstract class EnemyAITransitionCondition
    {
        public abstract bool CheckCondition(EnemyController sourceEnemy);
    }
}