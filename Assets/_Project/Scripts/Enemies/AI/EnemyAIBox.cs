﻿using System;
using TorkFramework.Serialization;

namespace Kairos.Enemies.AI
{
    public class EnemyAIBox
    {
        public enum BoxLoopType
        {
            Loop,
            PopAfterOne,
            PopAfterLoop
        }
        
        [XmlSerialized] private EnemyAITurnAction[] m_TurnSequence;
        [XmlSerialized] private BoxLoopType m_LoopType;
        
        private int m_ActualIndex;
        
        public EnemyAITurnAction[] TurnSequence
        {
            get => m_TurnSequence;
            set => m_TurnSequence = value;
        }

        public BoxLoopType LoopType
        {
            get => m_LoopType;
            set => m_LoopType = value;
        }

        public EnemyAIBox()
        {
            m_TurnSequence = new EnemyAITurnAction[0];
        }
        
        public EnemyAITurnAction GetTurn(out bool pop)
        {
            var result = m_TurnSequence[m_ActualIndex];
            m_ActualIndex++;
            if (m_ActualIndex >= m_TurnSequence.Length)
            {
                m_ActualIndex = 0;
            }

            switch (m_LoopType)
            {
                case BoxLoopType.Loop:
                {
                    pop = false;
                    break;
                }
                case BoxLoopType.PopAfterOne:
                {
                    pop = true;
                    break;
                }
                case BoxLoopType.PopAfterLoop:
                {
                    if (m_ActualIndex == 0)
                        pop = true;
                    else
                        pop = false;
                    break;
                }
                default:
                {
                    pop = false;
                    break;
                }
            }

            return result;
        }

        public void Reset()
        {
            m_ActualIndex = 0;
        }

        public void Resize(int newSize)
        {
            if (newSize > m_TurnSequence.Length)
            {
                var actualSize = m_TurnSequence.Length;
                Array.Resize(ref m_TurnSequence, newSize);
                for (int i = actualSize; i < m_TurnSequence.Length; i++)
                {
                    m_TurnSequence[i] = new EnemyAITurnAction();
                }
            }
            Array.Resize(ref m_TurnSequence, newSize);
        }
    }
}