﻿using System;
using Kairos.Cards;
using TorkFramework.Serialization;

namespace Kairos.Enemies.AI
{
    public class EnemyAITurnAction
    {
        [XmlSerialized, Card] private string[] m_TurnCards;
        
        public string[] TurnCards
        {
            get { return m_TurnCards; }
            set { m_TurnCards = value; }
        }

        public EnemyAITurnAction()
        {
            m_TurnCards = new string[0];
        }
        
        public void Resize(int newSize)
        {
            if (newSize > m_TurnCards.Length)
            {
                var actualSize = m_TurnCards.Length;
                Array.Resize(ref m_TurnCards, newSize);
                for (int i = actualSize; i < m_TurnCards.Length; i++)
                {
                    m_TurnCards[i] = "";
                }
            }
            Array.Resize(ref m_TurnCards, newSize);
        }
    }
}