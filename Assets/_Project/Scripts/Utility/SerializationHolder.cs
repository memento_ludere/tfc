﻿using TorkFramework.Serialization;

namespace Kairos
{
    public class SerializationHolder<T>
    {
        [XmlSerialized] public T SerializedObject;
    }
}