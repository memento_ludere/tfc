﻿using Kairos.UI.Cards;
using TorkFramework.ResourceManagement;
using UnityEditor;
using UnityEngine;

namespace Kairos.Utility.Editor
{
    [CreateAssetMenu]
    public class GlobalEditorSettings : ScriptableObject
    {
        #region Singleton Pattern

        private static GlobalEditorSettings m_Instance;

        public static GlobalEditorSettings Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = Resources.Load<GlobalEditorSettings>("GlobalEditorSettings");

                    // If is called by editor and there's no PrefabDatabase asset file, then create it
#if UNITY_EDITOR
                    if (m_Instance == null)
                    {
                        if (!System.IO.Directory.Exists(Application.dataPath + "/_Project"))
                            System.IO.Directory.CreateDirectory(Application.dataPath + "/_Project");

                        if (!System.IO.Directory.Exists(Application.dataPath + "/_Project/Resources"))
                            System.IO.Directory.CreateDirectory(Application.dataPath + "/_Project/Resources");

                        AssetDatabase.CreateAsset(CreateInstance<GlobalEditorSettings>(), "Assets/_Project/Resources/GlobalEditorSettings.asset");
                        m_Instance = Resources.Load<GlobalEditorSettings>("GlobalEditorSettings");
                    }
#endif
                }

                return m_Instance;
            }
        }

        #endregion

        public UICardView CardPreview;
    }
}