﻿using System;
using System.Collections.Generic;
using Kairos.Heroes;
using TorkFramework.Serialization;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Kairos.Progression.Rewards
{
    [CreateAssetMenu(menuName = "Reward Pool")]
    public class RewardPool : ScriptableObject
    {
        public RewardObject[] PossibleRewards;

        public RewardObject[] GetValuableRewards(HeroInfo targetHero)
        {
            var rewards = new List<RewardObject>();
            foreach (var possibleReward in PossibleRewards)
            {
                if (string.IsNullOrEmpty(possibleReward.Reward.Hero.Id) || targetHero.HeroId == possibleReward.Reward.Hero.Id)
                {
                    rewards.Add(possibleReward);
                }
            }

            if (rewards == null)
            {
                throw new Exception("Unable to find valid rewards");
            }

            return rewards.ToArray();
        }

        public Reward GetRandomReward(HeroInfo targetHero)
        {
            var rewards = new List<RewardObject>();
            foreach (var possibleReward in PossibleRewards)
            {
                if (string.IsNullOrEmpty(possibleReward.Reward.Hero.Id) || targetHero.HeroId == possibleReward.Reward.Hero.Id)
                {
                    rewards.Add(possibleReward);
                }
            }

            if (rewards == null)
            {
                throw new Exception("Unable to find valid rewards");
            }

            return rewards[Random.Range(0, rewards.Count)].Reward;
        }
    }
}