﻿using System;
using Kairos.Cards;
using Kairos.Player;
using Kairos.UI.Rewards;
using TorkFramework;
using TorkFramework.Runtime.UI;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Progression.Rewards
{
    public class CardReward : Reward
    {
        [XmlSerialized] public CardReference TargetCard;

        public CardReward()
        {
            TargetCard = new CardReference();
        }

        public override void CollectRewardFromShop(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback)
        {
            player.Data.AddCard(TargetCard.Id);
            rewardCollectedCallback?.Invoke();
        }

        public override void CollectReward(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback)
        {
            (gameManager.GetManager<UIManager>().QuerableEntities[typeof(UICardRewardPopup)] as UICardRewardPopup).ShowCardReward(TargetCard.Card, result =>
            {
                if (result)
                    player.Data.AddCard(TargetCard.Id);
                rewardCollectedCallback?.Invoke();
            });
        }
    }
}