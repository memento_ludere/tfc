﻿using System;
using System.Collections;
using System.Collections.Generic;
using Kairos.Heroes;
using Kairos.Player;
using TorkFramework;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Progression.Rewards
{
    public class PotionReward : Reward
    {
        [XmlSerialized] private int RefillCount;
        [XmlSerialized] private bool Heal;

        public PotionReward()
        {
        }

        public override void CollectRewardFromShop(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback)
        {
            player.Data.PotionCount = Mathf.Clamp(player.Data.PotionCount + RefillCount, 0, HeroDatabase.LoadedHeroes[player.Data.HeroId].PotionCount);
            if(Heal)
                player.Data.Hero.ActualLifePoints += HeroDatabase.LoadedHeroes[ player.Data.HeroId].PotionHealingAmount;
            rewardCollectedCallback?.Invoke();
        }

        public override void CollectReward(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback)
        {
            player.Data.PotionCount = Mathf.Clamp(player.Data.PotionCount + RefillCount, 0, HeroDatabase.LoadedHeroes[player.Data.HeroId].PotionCount);
            if(Heal)
                player.Data.Hero.ActualLifePoints += HeroDatabase.LoadedHeroes[ player.Data.HeroId].PotionHealingAmount;
            rewardCollectedCallback?.Invoke();
        }
    }
}