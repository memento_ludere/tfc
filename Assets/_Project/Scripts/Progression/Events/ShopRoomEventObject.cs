﻿using System.Xml;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Progression.Events
{
    [CreateAssetMenu(menuName = "Events/Shop Room")]
    public class ShopRoomEventObject : ScriptableObject, IEventObject, ISerializationCallbackReceiver
    {
        [SerializeField] private string m_SerializedEvent;

        private ShopRoomEvent m_Event;

        public ShopRoomEvent Event
        {
            get
            {
                if(m_Event == null)
                    Load();
                return m_Event;
            }
            set
            {
                m_Event = value;
            }
        }
        
        public void Load()
        {
            if (string.IsNullOrEmpty(m_SerializedEvent))
            {
                m_Event = new ShopRoomEvent();
            }
            else
            {
                var deserializer = new Deserializer();
                m_Event = deserializer.DeserializeFromString<ShopRoomEvent>(m_SerializedEvent);
            }
        }

        public void Save()
        {
            if(m_Event == null)
                Load();
            
            var serializer = new Serializer(new XmlWriterSettings());
            m_SerializedEvent = serializer.SerializeToString(m_Event);
        }

        public void OnBeforeSerialize()
        {
            Save();
        }

        public void OnAfterDeserialize()
        {
            Load();
        }

        public TierChangeEvent GetEvent()
        {
            return Event;
        }
    }
}