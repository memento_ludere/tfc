﻿using System;
using TorkFramework;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;

namespace Kairos.Progression.Encounters
{
    public abstract class Encounter
    {
        [XmlSerialized] public ResourceReference PreviewSprite;
        [XmlSerialized] public int Cost;

        public Encounter()
        {
            Cost = 0;
            PreviewSprite = new ResourceReference();
        }

        public Encounter(Encounter source)
        {
            Cost = source.Cost;
            PreviewSprite = new ResourceReference {Path = source.PreviewSprite.Path};
        }
        
        public abstract void GenerateEncounter(GameManager gameManager);
        public abstract void PlayEncounter(GameManager gameManager, Action<bool> encounterExecutedCallback);
    }
}