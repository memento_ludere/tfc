﻿using System;
using Kairos.Player;
using TorkFramework;
using TorkFramework.Serialization;

namespace Kairos.Progression.Tree
{
    public class HealthChangeTreeEffect : TreeEffect
    {
        [XmlSerialized] public int HealthChange;

        public HealthChangeTreeEffect() : base()
        {
            
        }

        public HealthChangeTreeEffect(HealthChangeTreeEffect source) : base(source)
        {
            HealthChange = source.HealthChange;
        }
        
        public override void Generate(GameManager gameManager)
        {
        }

        public override void Execute(GameManager gameManager, Action<bool> effectExecutedCallback)
        {
            var actualHealthPoints = gameManager.GetManager<PlayerManager>().Data.Hero.ActualLifePoints;
            gameManager.GetManager<PlayerManager>().Data.Hero.ActualLifePoints = actualHealthPoints + HealthChange <= 0 ? 1 : actualHealthPoints + HealthChange;
            effectExecutedCallback?.Invoke(true);
        }
    }
}