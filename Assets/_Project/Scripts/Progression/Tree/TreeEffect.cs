﻿using System;
using TorkFramework;

namespace Kairos.Progression.Tree
{
    public abstract class TreeEffect
    {
        public TreeEffect() { }

        public TreeEffect(TreeEffect source) { }
        public abstract void Generate(GameManager gameManager);
        public abstract void Execute(GameManager gameManager, Action<bool> effectExecutedCallback);
    }
}