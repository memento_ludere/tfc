﻿using System;
using Kairos.Player;
using Kairos.Progression.Rewards;
using TorkFramework;
using TorkFramework.Serialization;

namespace Kairos.Progression.Tree
{
    public class CardTreeEffect : TreeEffect
    {
        [XmlSerialized] public CardReward Reward;

        public CardTreeEffect() : base()
        {
            Reward = new CardReward();
        }

        public CardTreeEffect(CardTreeEffect source) : base(source)
        {
            Reward = new CardReward{TargetCard = source.Reward.TargetCard};
        }
        
        public override void Generate(GameManager gameManager) { }

        public override void Execute(GameManager gameManager, Action<bool> effectExecutedCallback)
        {
            Reward.CollectReward(gameManager, gameManager.GetManager<PlayerManager>(), () => effectExecutedCallback?.Invoke(true));
        }
    }
}