﻿using Kairos.Sentinels;
using Kairos.UI.Feedbacks;
using Kairos.UI.Player;
using Kairos.UI.Timeline;
using System;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

public class UISentinelView : EntityBehaviour<UIEntity>
{
    private SentinelController m_AssignedSentinel;
    private UITimelineHighlighter m_TimelineHighlighter;
    [SerializeField] private GameObject[] m_ActivationGemsCounter;
    [SerializeField] private Image m_SentinelIcon;
    [SerializeField] private Sprite m_ActiveCounter;
    [SerializeField] private Sprite m_InactiveCounter;
    [SerializeField] private UITooltipComponent m_TooltipComponent;
    private ButtonState m_CurrentState;

    private void Update()
    {
        UpdateActivationsView();

        if (Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyDown(KeyCode.Escape))
        {
            DeselectSentinel();
        }
    }

    public enum ButtonState
    {
        Disabled,
        Active,
        SentinelPlaced,
        SentinelSelected
    }

    public void OnSentinelPlaced(GenericEventArgs args, Action eventCallback)
    {
        if((SentinelController)args.Args[0] == m_AssignedSentinel)
        {
            m_CurrentState = ButtonState.SentinelPlaced;
            m_AssignedSentinel.CurrentActivations--;
        }
        eventCallback?.Invoke();
    }

    public void SelectSentinel()
    {
        if (m_AssignedSentinel.CurrentActivations > 0 && 
            m_CurrentState == ButtonState.Active && 
            m_TimelineHighlighter.CurrentStatus == UITimelineHighlighter.SentinelSelectionStatus.Disabled &&
            (Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIPlayerController)] as UIPlayerController).ControllerState == UIPlayerController.UIPlayerControllerState.Idle)
        {
            UIPlayerController controller = Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIPlayerController)] as UIPlayerController;
            if (controller.ControllerState == UIPlayerController.UIPlayerControllerState.Idle)
            {
                m_TimelineHighlighter.OnSentinelSelected(m_AssignedSentinel);
                controller.ControllerState = UIPlayerController.UIPlayerControllerState.PlacingSentinel;
            }
            //m_CurrentState = ButtonState.SentinelSelected;
        }
    }

    public void DeselectSentinel()
    {
        if (m_AssignedSentinel.CurrentActivations > 0 &&  
            m_TimelineHighlighter.CurrentStatus == UITimelineHighlighter.SentinelSelectionStatus.Selected &&
           (Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIPlayerController)] as UIPlayerController).ControllerState == UIPlayerController.UIPlayerControllerState.PlacingSentinel)
        {
            UIPlayerController controller = Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIPlayerController)] as UIPlayerController;
            if (controller.ControllerState == UIPlayerController.UIPlayerControllerState.PlacingSentinel)
            {
                m_TimelineHighlighter.Clear();
                controller.ControllerState = UIPlayerController.UIPlayerControllerState.Idle;
            }
            m_CurrentState = ButtonState.Active;
        }
    }

    private void UpdateActivationsView()
    {
        for (int i = 0; i < m_ActivationGemsCounter.Length; i++)
        {
            if(i < m_AssignedSentinel.CurrentActivations)
            {
                m_ActivationGemsCounter[i].SetActive(true);
                m_ActivationGemsCounter[i].GetComponent<Image>().sprite = m_ActiveCounter;
            }
            else
            {
                m_ActivationGemsCounter[i].GetComponent<Image>().sprite = m_InactiveCounter;
            }
        }

        if(m_AssignedSentinel.m_CurrentState == SentinelController.SentinelState.Active)
        {
            m_CurrentState = ButtonState.Active;
        }
    }

    public void SetupSentinelUIView(SentinelController sentinel, UITimelineHighlighter highlighter)
    {
        m_AssignedSentinel = sentinel;
        m_SentinelIcon.sprite = sentinel.SentinelData.SentinelIcon.Resource as Sprite;
        m_TimelineHighlighter = highlighter;
        m_CurrentState = ButtonState.Active;
        string title = "<b>" + sentinel.SentinelData.SentinelName + "</b>\n";
        string description = sentinel.SentinelData.SentinelDescription;
        string total = title + description;
        m_TooltipComponent.ShowText = total;
        for (int i = 0; i < m_ActivationGemsCounter.Length; i++)
        {
            m_ActivationGemsCounter[i].SetActive(false);
        }

        for(int i = 0; i < sentinel.SentinelData.MaxActivations; i++)
        {
            m_ActivationGemsCounter[i].SetActive(true);
            m_ActivationGemsCounter[i].GetComponent<Image>().sprite = m_ActiveCounter;
        }
    }

    public void TerminateSentinelUIView()
    {
        m_CurrentState = ButtonState.Disabled;
        for(int i = 0; i < m_ActivationGemsCounter.Length; i++)
        {
            m_ActivationGemsCounter[i].SetActive(false);
        }
        m_AssignedSentinel.TerminateSentinel();
    }

    public void OnPointerEnter()
    {
        m_TooltipComponent.Show();
    }

    public void OnPointerExit()
    {
        m_TooltipComponent.Hide();
    }
}
