﻿using System.Collections.Generic;
using Kairos.Player;
using Kairos.Sentinels;
using Kairos.UI.PartyCreation;
using Kairos.UI.Popups;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI
{
    public class StartGamePartyButton : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UIHeroSelector m_StartHeroSelector;
        
        public void StartGame()
        {
            Entity.UIManager.GetQuerableEntity<SingleQuestionPopupController>().ShowConfirmBox("Are you sure to start with this Hero?", "Yes", "No", (result) =>
                {
                    if (result)
                    {
                        Entity.GameManager.GetManager<PlayerManager>().StartNewGame(new PlayerData(m_StartHeroSelector.SelectedHero, new List<SentinelInfo>()));
                    }
                }
            );
        }
    }
}