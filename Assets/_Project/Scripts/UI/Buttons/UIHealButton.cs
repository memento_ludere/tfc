﻿using Kairos.Player;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI
{
    public class UIHealButton : EntityBehaviour<UIEntity>
    {
        [SerializeField] private Button m_PotionButton;
        [SerializeField] private TextMeshProUGUI m_PotionCountText;
        
        public void OnClick()
        {
            Entity.GameManager.GetManager<PlayerManager>().UsePotion();
        }

        private void Update()
        {
            if (Entity.GameManager.GetManager<PlayerManager>().Data.PotionCount <= 0)
            {
                m_PotionButton.enabled = false;
                m_PotionCountText.text = "0";
                return;
            }
            
            m_PotionButton.enabled = true;
            m_PotionCountText.text = $"{Entity.GameManager.GetManager<PlayerManager>().Data.PotionCount}";
        }
    }
}