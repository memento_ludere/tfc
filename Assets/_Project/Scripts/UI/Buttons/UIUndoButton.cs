﻿using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;

namespace Kairos.UI
{
    public class UIUndoButton : EntityBehaviour<UIEntity>
    {
        public void OnClick()
        {
            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UndoRequest, this);
        }
    }
}