﻿using Kairos.Progression;
using TorkFramework;
using TorkFramework.Runtime.UI;

namespace Kairos.UI
{
    public class ContinueButton : EntityBehaviour<UIEntity>
    {
        private void Update()
        {
            CheckEnabled();
        }

        private void CheckEnabled()
        {
            if (!Entity.GameManager.GetManager<ProgressionManager>().CanContinue)
            {
                gameObject.SetActive(false);
            }
        }

        public void OnClick()
        {
            Entity.GameManager.GetManager<ProgressionManager>().Continue(() => { Entity.GameManager.ChangeState(GameStates.LevelSelection); });
        }
    }
}