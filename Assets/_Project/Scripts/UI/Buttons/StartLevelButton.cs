﻿using Kairos.Managers;
using TorkFramework;
using TorkFramework.Runtime.UI;

namespace Kairos.UI
{
    public class StartLevelButton : EntityBehaviour<UIEntity>
    {
        public void SelectLevel()
        {
            Entity.GameManager.GetManager<LevelManager>().StartLevel();
        }
    }
}