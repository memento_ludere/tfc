﻿using TorkFramework;
using TorkFramework.Runtime.UI;

namespace Kairos.UI
{
    public class UIMainMenuButton : EntityBehaviour<UIEntity>
    {
        public void OnClick()
        {
            Entity.GameManager.ChangeState(GameStates.MainMenu);
        }
    }
}