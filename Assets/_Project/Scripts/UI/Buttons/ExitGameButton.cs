﻿using Kairos.UI.Popups;
using TorkFramework;
using TorkFramework.Runtime.UI;
namespace Kairos.UI
{
    public class ExitGameButton : EntityBehaviour<UIEntity>
    {
        public void ExitGame()
        {
            (Entity.UIManager.QuerableEntities[typeof(SingleQuestionPopupController)] as SingleQuestionPopupController).ShowConfirmBox("Exit game?", "Yes", "No", (result) =>
                {
                    if (result)
                    {
#if UNITY_EDITOR
                        UnityEditor.EditorApplication.ExitPlaymode();
#else
                        UnityEngine.Application.Quit();
#endif
                    }
                }
            );
        }
    }
}