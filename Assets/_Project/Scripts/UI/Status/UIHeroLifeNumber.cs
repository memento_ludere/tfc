﻿using System;
using Kairos.Player;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Status
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class UIHeroLifeNumber : EntityBehaviour<UIEntity>
    {
        private TextMeshProUGUI m_LifeText;

        private void Awake()
        {
            m_LifeText = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable()
        {
            UpdateLifeText();
        }

        private void Start()
        {
            UpdateLifeText();
        }

        private void UpdateLifeText()
        {
            if (Entity != null && Entity.GameManager != null)
            {
                if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
                {
                    m_LifeText.text = Entity.GameManager.GetManager<PlayerManager>().Data.Hero.ActualLifePoints + "/" + Entity.GameManager.GetManager<PlayerManager>().Data.Hero.TargetUnit.MaxLifePoints;
                }
            }
        }
    }
}