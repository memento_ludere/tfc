﻿using System;
using Kairos.Heroes;
using Kairos.Player;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Status
{
    [RequireComponent(typeof(Image))]
    public class UIHeroIllustration : EntityBehaviour<UIEntity>
    {
        private Image m_IllustrationImage;

        private void Awake()
        {
            m_IllustrationImage = GetComponent<Image>();
        }

        private void OnEnable()
        {
            UpdateIllustration();
        }
        
        private void Start()
        {
            UpdateIllustration();
        }

        private void UpdateIllustration()
        {
            if (Entity != null && Entity.GameManager != null)
            {
                if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
                {
                    try
                    {
                        m_IllustrationImage.sprite = Entity.GameManager.GetManager<PlayerManager>().Data.Hero.TargetUnit.UnitRender.Resource as Sprite;
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError("The render of hero is defined as a non sprite asset!");
                    }
                }
            }
        }
    }
}