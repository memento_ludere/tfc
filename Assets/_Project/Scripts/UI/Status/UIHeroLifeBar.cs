﻿using System;
using System.Collections;
using System.Collections.Generic;
using Kairos.Player;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Status
{
    [RequireComponent(typeof(Slider))]
    public class UIHeroLifeBar : EntityBehaviour<UIEntity>
    {
        private Slider m_LiferBarSlider;

        private void Awake()
        {
            m_LiferBarSlider = GetComponent<Slider>();
        }

        private void OnEnable()
        {
            UpdateLifeBar();
        }

        private void Start()
        {
            UpdateLifeBar();
        }

        private void UpdateLifeBar()
        {
            if (Entity != null && Entity.GameManager != null)
            {
                if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
                {
                    m_LiferBarSlider.maxValue = Entity.GameManager.GetManager<PlayerManager>().Data.Hero.TargetUnit.MaxLifePoints;
                    m_LiferBarSlider.value = Entity.GameManager.GetManager<PlayerManager>().Data.Hero.ActualLifePoints;
                }
            }
        }
    }
}