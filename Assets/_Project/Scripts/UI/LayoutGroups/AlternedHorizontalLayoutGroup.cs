﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlternedHorizontalLayoutGroup : LayoutGroup
{
    [SerializeField] private float m_ElementSpreadOffset;
    [SerializeField] private float m_ElementSpaceOffset;
    [SerializeField] private Vector2 m_ChildSize;

    public override void CalculateLayoutInputVertical()
    {
    }

    public override void SetLayoutHorizontal()
    {
        SetCellsAlongAxis(1);
    }

    public override void SetLayoutVertical()
    {
        SetCellsAlongAxis(0);
    }

    private void SetCellsAlongAxis(int axis)
    {
        int index = 0;
        foreach (Transform child in transform)
        {
            child.transform.localPosition = CalculateOffsetPosition(axis, index);
            (child.transform as RectTransform).SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_ChildSize.x);
            (child.transform as RectTransform).SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, m_ChildSize.y);
            index++;
        }
    }

    private Vector3 CalculateOffsetPosition(int axys, int index)
    {
        int spreadMultiplier = 1;

        if ((float) index % 2f == 0f)
            spreadMultiplier = 0;

        if (axys == 0)
            return new Vector3(index * m_ElementSpaceOffset, m_ElementSpreadOffset * spreadMultiplier, 0f);
        return new Vector3(m_ElementSpreadOffset * spreadMultiplier, index * m_ElementSpaceOffset, 0f);
    }
}