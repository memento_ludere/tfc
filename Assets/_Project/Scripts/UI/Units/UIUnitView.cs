﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Kairos.Statuses;
using Kairos.Timeline;
using Kairos.UI.Feedbacks;
using Kairos.Units;
using TMPro;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Units
{
    public class UIUnitView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private TextMeshProUGUI m_UnitName;
        [SerializeField] private Image m_UnitPortrait;
        [SerializeField] private TextMeshProUGUI m_LifeText;
        [SerializeField] private Slider m_LifeSlider;
        [SerializeField] private TextMeshProUGUI m_PreviewLifeText;
        [SerializeField] private Slider m_PreviewLifeSlider;
        [SerializeField] private Slider m_PreviewLifeSliderMask;
        [SerializeField] private Transform m_StatusGroup;
        [SerializeField] private UIStatusView m_StatusViewPrefab;
        [SerializeField] private Image m_UnitIcon;
        [SerializeField] private UIDamagePopQueue m_PopQueuePrefab;
        [SerializeField] private Vector3 m_PopQueueOffset = Vector3.up * .5f;

        private UnitController m_ReferencedUnit;
        private UIDamagePopQueue m_PopQueue;
        public TimelineController Timeline;

        public UnitController ReferencedUnit
        {
            get => m_ReferencedUnit;
            set
            {
                m_ReferencedUnit = value;
                if (m_ReferencedUnit != null)
                {
                    m_LifeSlider.maxValue = m_ReferencedUnit.ReferencedUnit.TargetUnit.MaxLifePoints;
                    m_LifeSlider.value = m_ReferencedUnit.ReferencedUnit.ActualLifePoints;
                    m_PreviewLifeSliderMask.maxValue = m_LifeSlider.maxValue;
                    m_PreviewLifeSliderMask.value = m_LifeSlider.value;
                    m_PreviewLifeSlider.maxValue = m_ReferencedUnit.ReferencedUnit.TargetUnit.MaxLifePoints;
                    m_PreviewLifeSlider.value = m_ReferencedUnit.ReferencedUnit.ActualLifePoints;

                    UpdateStats(() => { });

                    var components = transform.parent.GetComponentsInChildren<UIUnitView>();
                    Array.Sort(components, (firstView, secondView) => secondView.ReferencedUnit.transform.position.z.CompareTo(firstView.ReferencedUnit.transform.position.z));
                    for (int i = 0; i < components.Length; i++)
                    {
                        components[i].transform.SetSiblingIndex(i);
                    }

                    LayoutRebuilder.MarkLayoutForRebuild(transform.parent as RectTransform);
                }
            }
        }

        private void Awake()
        {
            var canvas = GameObject.FindWithTag("MasterCanvas");
            m_PopQueue = Entity.GameManager.Spawn<UIDamagePopQueue, UIEntity>(m_PopQueuePrefab);
            m_PopQueue.transform.SetParent(canvas.transform, false);
        }

        private void OnDestroy()
        {
            Entity.GameManager.Kill(m_PopQueue.Entity);
        }

        private void OnEnable()
        {
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.UnitStatsUpdated, OnUnitStatsUpdated);

            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.UnitTakeDamage, OnUnitTakeDamage);
        }

        private void OnDisable()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.UnitStatsUpdated, OnUnitStatsUpdated);

            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.UnitTakeDamage, OnUnitTakeDamage);
        }

        private void Update()
        {
            if (ReferencedUnit == null)
                return;

            var canvas = GameObject.FindWithTag("MasterCanvas");
            var viewportPosition = Camera.main.WorldToViewportPoint(m_ReferencedUnit.transform.position + m_PopQueueOffset);
            var canvasRect = canvas.transform as RectTransform;
            Vector2 worldObjectScreenPosition = new Vector2(
                (viewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * .5f),
                (viewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * .5f)
            );

            m_PopQueue.transform.localPosition = worldObjectScreenPosition;

            if (Timeline.Snapshots != null && Timeline.Snapshots.Length >= Globals.TIMELINE_SIZE + 1)
            {
                if (ReferencedUnit.Team == Team.Player)
                {
                    //m_PreviewLifeSlider.value = Timeline.Snapshots[Globals.TIMELINE_SIZE].HeroSnapshot.SourceUnit.ReferencedUnit.ActualLifePoints - Timeline.Snapshots[Globals.TIMELINE_SIZE].HeroSnapshot.Life;
                    m_PreviewLifeSlider.value = Timeline.Snapshots[Globals.TIMELINE_SIZE].HeroSnapshot.MaxLife - Timeline.Snapshots[Globals.TIMELINE_SIZE].HeroSnapshot.Life;
                    m_PreviewLifeText.text = (-(Timeline.Snapshots[Globals.TIMELINE_SIZE].HeroSnapshot.SourceUnit.ReferencedUnit.ActualLifePoints - Timeline.Snapshots[Globals.TIMELINE_SIZE].HeroSnapshot.Life)).ToString();
                }
                else
                {
                    foreach (var enemySnapshot in Timeline.Snapshots[Globals.TIMELINE_SIZE].EnemiesSnapshots)
                    {
                        if (enemySnapshot.SourceUnit == m_ReferencedUnit)
                        {
                            //m_PreviewLifeSlider.value = enemySnapshot.SourceUnit.ReferencedUnit.ActualLifePoints - enemySnapshot.Life;
                            m_PreviewLifeSlider.value = enemySnapshot.MaxLife - enemySnapshot.Life;
                            m_PreviewLifeText.text = (-(enemySnapshot.SourceUnit.ReferencedUnit.ActualLifePoints - enemySnapshot.Life)).ToString();
                        }
                    }
                }
            }
        }

        private void OnUnitStatsUpdated(GenericEventArgs args, Action listenerExecutedCallback)
        {
            var controller = (UnitController) args.Source;
            if (controller && m_ReferencedUnit && controller == m_ReferencedUnit)
            {
                UpdateStats(listenerExecutedCallback);
            }
            else
            {
                listenerExecutedCallback?.Invoke();
            }
        }

        private void UpdateStats(Action statsUpdatedCallback)
        {
            if (m_UnitName.text != ReferencedUnit.ReferencedUnit.TargetUnit.UnitName)
                m_UnitName.text = m_ReferencedUnit.ReferencedUnit.TargetUnit.UnitName;

            if (m_ReferencedUnit.ReferencedUnit.TargetUnit.UnitIcon != null && m_ReferencedUnit.ReferencedUnit.TargetUnit.UnitIcon.Resource != null)
            {
                if (m_UnitPortrait.sprite != m_ReferencedUnit.ReferencedUnit.TargetUnit.UnitIcon.Resource as Sprite)
                    m_UnitPortrait.sprite = m_ReferencedUnit.ReferencedUnit.TargetUnit.UnitIcon.Resource as Sprite;
            }
            else
            {
                m_UnitPortrait.sprite = null;
            }


            //m_UnitIcon.color = ReferencedUnit.GetUnitColor();
            m_UnitIcon.sprite = ReferencedUnit.GetUnitIcon();

            List<StatusInfo> status = StatusDatabase.GetUnitStatuses(ReferencedUnit);
            ClearGrid();
            if (status.Count > 0)
            {
                foreach (StatusInfo si in status)
                {
                    var statusPref = Entity.GameManager.Spawn<UIStatusView, UIEntity>(m_StatusViewPrefab);
                    statusPref.transform.SetParent(m_StatusGroup, false);
                    UIStatusView statusView = statusPref.GetComponent<UIStatusView>();
                    statusView.SetStatusView(si, si.ApplyCheck.GetStatusValue(ReferencedUnit));
                }

                LayoutRebuilder.MarkLayoutForRebuild(m_StatusGroup.transform as RectTransform);
            }

            var lifeText = m_ReferencedUnit.ReferencedUnit.ActualLifePoints.ToString();
            if (m_LifeText.text != lifeText)
            {
                m_LifeText.text = lifeText;
                DOTween.To(() => m_LifeSlider.value, x =>
                {
                    m_LifeSlider.value = x;
                    m_PreviewLifeSliderMask.value = x;
                }, m_ReferencedUnit.ReferencedUnit.ActualLifePoints, .5f).onComplete += () =>
                {
                    if (m_ReferencedUnit.ReferencedUnit.ActualLifePoints <= 0)
                        transform.gameObject.SetActive(false);
                    statsUpdatedCallback?.Invoke();
                };
            }
            else
            {
                statsUpdatedCallback?.Invoke();
            }
        }

        private void ClearGrid()
        {
            Entity.GameManager.KillChildren(m_StatusGroup.GetComponent<UIEntity>());
        }

        private void OnUnitTakeDamage(GenericEventArgs args, Action listenerExecutedCallback)
        {
            if (ReferencedUnit != null && (args.Source as UnitController) == ReferencedUnit)
            {
                var damage = (int) args.Args[0];
                m_PopQueue.PushDamage(damage);
            }

            listenerExecutedCallback?.Invoke();
        }
    }
}