﻿using System;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Popups
{
    public class MultipleQuestionPopupController : EntityBehaviour<UIPage>
    {
        [SerializeField] private TextMeshProUGUI m_ShowText;
        [SerializeField] private UIEntity m_ButtonList;
        [SerializeField] private UIEntity m_ButtonPrefab;

        private Action<int> m_ResultCallback;

        private void Start()
        {
            Entity.UIManager.AddQuerableEntity(this);
        }

        private void OnDestroy()
        {
            Entity.UIManager.ClearQuerableEntity(GetType());
        }

        public void ShowMultipleQuestionBox(string questionText, Action<int> answerSelectedCallback, params string[] options)
        {
            m_ShowText.text = questionText;
            m_ResultCallback = answerSelectedCallback;
            for (int i = 0; i < options.Length; i++)
            {
                var index = i;
                var newButton = Entity.GameManager.Spawn(m_ButtonPrefab);
                newButton.transform.SetParent(m_ButtonList.transform, false);
                newButton.GetComponentInChildren<TextMeshProUGUI>().text = options[index];
                newButton.GetComponent<Button>().onClick.AddListener(() => { OnButtonClicked(index); });
            }

            LayoutRebuilder.MarkLayoutForRebuild(m_ButtonList.transform as RectTransform);
            
            Entity.GameManager.ChangeState(GameStates.ConfirmMultiplePopup);
        }

        private void OnDisable()
        {
            Entity.GameManager.KillChildren(m_ButtonList);
        }

        private void OnButtonClicked(int buttonIndex)
        {
            Entity.GameManager.PopState(() =>
            {
                m_ResultCallback?.Invoke(buttonIndex);
                m_ResultCallback = null;
            });
        }
    }
}