﻿using Kairos.Keywords;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Keywords
{
    public class UIKeywordExplanationView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private TextMeshProUGUI m_NameText;
        [SerializeField] private TextMeshProUGUI m_DescriptionText;

        private string m_Keyword;

        public string Keyword
        {
            get => m_Keyword;
            set
            {
                m_Keyword = value;
                m_NameText.text = KeywordDatabase.LoadedKeywords[m_Keyword].KeywordStyleOpener + KeywordDatabase.LoadedKeywords[m_Keyword].KeywordText + KeywordDatabase.LoadedKeywords[m_Keyword].KeywordStyleCloser;
                m_DescriptionText.text = KeywordDatabase.LoadedKeywords[m_Keyword].KeywordDescription;
            }
        }
    }
}