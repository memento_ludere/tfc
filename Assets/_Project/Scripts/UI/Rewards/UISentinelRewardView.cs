﻿using Kairos.Player;
using Kairos.Sentinels;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

public class UISentinelRewardView : EntityBehaviour<UIEntity>
{
    [SerializeField] private TextMeshProUGUI m_SentinelNameText;
    [SerializeField] private TextMeshProUGUI m_SentinelDescriptionText;
    [SerializeField] private Image m_SentinelIcon;
    [SerializeField] private GameObject[] m_SentinelActivations;
    [SerializeField] private GameObject m_UnavailableSentinelText;

    public void SetupSentinelPreview(SentinelInfo data)
    {
        m_SentinelNameText.text = data.SentinelName;
        m_SentinelDescriptionText.text = data.SentinelDescription;
        m_SentinelIcon.sprite = data.SentinelIcon.Resource as Sprite;

        for(int i = 0; i < m_SentinelActivations.Length; i++)
        {
            m_SentinelActivations[i].SetActive(false);
        }

        for(int i = 0; i < data.MaxActivations; i++)
        {
            m_SentinelActivations[i].SetActive(true);
        }

        if(Entity.GameManager.GetManager<PlayerManager>().Data.Sentinels.Contains(data))
		{
            m_UnavailableSentinelText.SetActive(true);
        }
        else
		{
            m_UnavailableSentinelText.SetActive(false);
        }
    }

    public bool IsSentinelAlreadyOwned(SentinelInfo data)
	{
        if (Entity.GameManager.GetManager<PlayerManager>().Data.Sentinels.Contains(data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
