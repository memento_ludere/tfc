﻿using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

public class UITooltipController : EntityBehaviour<UIEntity>
{
    [SerializeField] private GameObject m_TooltipPanel;
    [SerializeField] private TextMeshProUGUI m_TooltipText;
    [SerializeField] private RectTransform[] m_ValuableSpots;

    private Camera m_UICamera;

    public Camera UiCamera
    {
        get
        {
            if (m_UICamera == null)
                m_UICamera = GameObject.FindWithTag("UICamera").GetComponent<Camera>();
            return m_UICamera;
        }
    }
    
    private void Start()
    {
        Entity.UIManager.AddQuerableEntity(this);
    }

    private void OnDestroy()
    {
        Entity.UIManager.ClearQuerableEntity(typeof(UITooltipController));
    }

    public void ShowTooltip(string text)
    {
        m_TooltipText.text = text;
        m_TooltipPanel.SetActive(true);
    }

    public void HideTooltip()
    {
        m_TooltipPanel.SetActive(false);
    }

    private void Update()
    {
        if (m_TooltipPanel.activeInHierarchy)
        {
            CalculatePosition();
        }
    }

    private void CalculatePosition()
    {
        var canvas = transform.parent;
        var viewportPosition = UiCamera.ScreenToViewportPoint(Input.mousePosition);
        var canvasRect = canvas.transform as RectTransform;
        Vector2 calculatedPosition = new Vector2(
            (viewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * .5f),
            (viewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * .5f)
        );
        transform.localPosition = calculatedPosition;
        
        foreach (var valuableSpot in m_ValuableSpots)
        {
            var viewportMin = UiCamera.ScreenToViewportPoint((transform as RectTransform).anchoredPosition + valuableSpot.anchoredPosition);
            var viewportMax = UiCamera.ScreenToViewportPoint((transform as RectTransform).anchoredPosition + valuableSpot.anchoredPosition + new Vector2(valuableSpot.rect.width, -valuableSpot.rect.height));

            if (viewportMin.x >= 0 && viewportMin.y >= 0 && viewportMin.x <= 1 && viewportMin.y <= 1 && viewportMax.x >= 0 && viewportMax.y >= 0 && viewportMax.x <= 1 && viewportMax.y <= 1)
            {
                m_TooltipPanel.transform.localPosition = valuableSpot.localPosition;
                break;
            }
        }
    }
}