﻿using Kairos.Heroes;
using UnityEngine;

namespace Kairos.Defaults
{
    [CreateAssetMenu]
    public class DefaultData : ScriptableObject
    {
        [SerializeField, Hero] private string[] m_UnlockedHeroes;

        public string[] UnlockedHeroes => m_UnlockedHeroes;
    }
}