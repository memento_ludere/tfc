﻿using System;
using Kairos.Controllers;
using Kairos.Enemies;
using Kairos.Enemies.EnemyPools;
using Kairos.Player;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Scenes;
using UnityEngine;

namespace Kairos.Managers
{
    public class RoundManager : Manager
    {
        [SerializeField] private BoardController m_BoardPrefab;
        [SerializeField] private TimelineController m_TimelinePrefab;
        [SerializeField] private PlayerController m_PlayerPrefab;
        [SerializeField] private EnemyTeamController m_EnemyTeamPrefab;
        [SerializeField] private SelectionController m_SelectionPrefab;

        private bool m_RoundLoaded;
        private BoardController m_Board;
        private TimelineController m_Timeline;
        private PlayerController m_Player;
        private EnemyTeamController m_EnemyTeam;
        private SelectionController m_Selection;

        private Action<bool> m_RoundCompleted;

        public bool IsLoaded => m_RoundLoaded;
        public PlayerController Player => m_Player;
        public BoardController Board => m_Board;

        public void StartNewRound(EnemyPoolReference enemies, Action<bool> roundCompleted)
        {
            if (m_RoundLoaded)
            {
                EndRound(() => StartNewRound(enemies, roundCompleted));
            }
            else
            {
                m_RoundCompleted = roundCompleted;
                GameManager.GetManager<EventManager>().AddListener(EventId.UnitDeath, OnUnitDeath);

                GameManager.ChangeState(GameStates.Loading, () =>
                {
                    GameManager.GetManager<SceneManager>().LoadScene(Globals.GAME_SCENE, () =>
                    {
                        GameManager.GetManager<SceneManager>().SetActiveScene(Globals.GAME_SCENE);

                        m_Board = GameManager.Spawn<BoardController, GameEntity>(m_BoardPrefab);
                        m_Selection = GameManager.Spawn<SelectionController, GameEntity>(m_SelectionPrefab);
                        m_Timeline = GameManager.Spawn<TimelineController, GameEntity>(m_TimelinePrefab);
                        m_EnemyTeam = GameManager.Spawn<EnemyTeamController, GameEntity>(m_EnemyTeamPrefab);
                        m_Player = GameManager.Spawn<PlayerController, GameEntity>(m_PlayerPrefab);

                        m_Timeline.Load(m_Board, m_Selection);

                        m_EnemyTeam.Load(m_Board, m_Timeline, enemies, () =>
                        {

                            m_Player.Load(m_Board, m_Timeline, GameManager.GetManager<PlayerManager>().Data, () =>
                            {
                                GameManager.ChangeState(GameStates.InGame, () =>
                                {
                                    GameManager.GetManager<EventManager>().TriggerEvent(EventId.RoundStarted, this, () =>
                                {
                                    m_RoundLoaded = true;
                                    StartTurn();
                                });
                                });
                            });
                        });
                    });
                });
            }
        }

        public void EndRound(Action roundEnded)
        {
            if (!m_RoundLoaded)
            {
                roundEnded?.Invoke();
                return;
            }

            GameManager.GetManager<EventManager>().RemoveListener(EventId.TimelineEndReached, OnTimelineEndReached);
            GameManager.GetManager<EventManager>().RemoveListener(EventId.UnitDeath, OnUnitDeath);

            m_EnemyTeam.Unload();
            GameManager.Kill(m_EnemyTeam.Entity);
            m_EnemyTeam = null;

            m_Player.Unload();
            GameManager.Kill(m_Player.Entity);
            m_Player = null;

            m_Timeline.Unload();
            GameManager.Kill(m_Timeline.Entity);
            m_Timeline = null;

            GameManager.Kill(m_Board.Entity);
            m_Board = null;

            GameManager.GetManager<SceneManager>().UnloadScene((Globals.GAME_SCENE), () =>
            {
                m_RoundLoaded = false;
                GameManager.GetManager<EventManager>().TriggerEvent(EventId.RoundEnded, this, roundEnded);
            });
        }

        private void StartTurn()
        {
            m_Timeline.Clear();

            m_EnemyTeam.OnTurnStarted(() =>
            {
                m_Player.OnTurnStarted(() =>
                {
                    GameManager.GetManager<EventManager>().TriggerEvent(EventId.TurnStarted, this, () => { GameManager.GetManager<EventManager>().AddListener(EventId.TimelineEndReached, OnTimelineEndReached); });
                });
            });
        }

        private void EndTurn()
        {
            GameManager.GetManager<EventManager>().RemoveListener(EventId.TimelineEndReached, OnTimelineEndReached);
            m_Player.OnTurnEnd(() =>
            {
                GameManager.GetManager<EventManager>().TriggerEvent(EventId.TurnEnded, this, StartTurn);
            });
        }

        private void OnUnitDeath(GenericEventArgs args, Action listenerExecutedCallback)
        {
            m_Timeline.RemoveUnitActions(args.Source as UnitController);
            if (m_Board.HeroUnit.IsDead)
            {
                EndRound(() =>
                {
                    GameManager.GetManager<EventManager>().TriggerEvent(EventId.RoundLost, this);
                    m_RoundCompleted?.Invoke(false);
                    m_RoundCompleted = null;
                });
            }
            else
            {
                var enemiesCount = 0;
                for (var i = 0; i < m_Board.EnemyUnits.Length; i++)
                {
                    if (m_Board.EnemyUnits[i] != null && !m_Board.EnemyUnits[i].IsDead)
                        enemiesCount++;
                }

                if (enemiesCount <= 0)
                {
                    EndRound(() =>
                    {
                        GameManager.GetManager<EventManager>().TriggerEvent(EventId.RoundWin, this);
                        m_RoundCompleted?.Invoke(true);
                        m_RoundCompleted = null;
                    });
                }
                else
                {
                    listenerExecutedCallback?.Invoke();
                }
            }
        }

        private void OnTimelineEndReached(GenericEventArgs args, Action listenerExecutedCallback)
        {
            listenerExecutedCallback?.Invoke();
            EndTurn();
        }
    }
}