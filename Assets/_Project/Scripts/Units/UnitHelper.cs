﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using TorkFramework.Serialization;
using UnityEditor;

namespace Kairos.Units
{
    public static class UnitHelper
    {
        public static void SaveUnit(UnitInfo unit, string path)
        {
            #if UNITY_EDITOR
            if (!Directory.Exists(Path.GetDirectoryName(path)))
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            
            var serializer = new Serializer(new XmlWriterSettings());
            serializer.SerializeToFile(path, unit);
            
            AssetDatabase.Refresh();
            #endif
        }

        public static UnitInfo LoadUnit(string path)
        {
            if(!File.Exists(path))
                throw new FileNotFoundException();
            
            if(Path.GetExtension(path).ToLower() != ".unit")
                throw new BadImageFormatException();
            
            var deserializer = new Deserializer();
            return deserializer.DeserializeFromFile<UnitInfo>(path);
        }

        private static string GenerateUnitId()
        {
            var result = default(byte[]);

            using (var stream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
                {
                    writer.Write(DateTime.Now.Ticks);
                }

                stream.Position = 0;

                using (var hash = SHA256.Create())
                {
                    result = hash.ComputeHash(stream);
                }
            }

            var resultText = "";
            for (int i = 0; i < result.Length; i++)
            {
                resultText += result[i].ToString("x2");
            }

            return "unit-" + resultText;
        }

        public static UnitInfo GenerateNewUnit()
        {
            return new UnitInfo {UnitID = GenerateUnitId()};
        }
    }
}