﻿using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;

namespace Kairos.Units
{
    public class UnitInfo
    {
        //General
        [XmlSerialized] public string UnitID;
        [XmlSerialized] public string UnitName;
        [XmlSerialized] public string Lore;

        //Stats
        [XmlSerialized] public int MaxLifePoints;

        //View
        [XmlSerialized] public ResourceReference UnitPrefab;
        [XmlSerialized] public ResourceReference UnitRender;
        [XmlSerialized] public ResourceReference UnitIcon;
        [XmlSerialized] public ResourceReference HeroIcon;
    }
}