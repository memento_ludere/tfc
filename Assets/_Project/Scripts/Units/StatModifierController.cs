﻿using System.Collections.Generic;

namespace Kairos.Units
{
    public class StatModifierController
    {
        private class StatModifierValue
        {
            public readonly int Amount;
            public int Time;

            public StatModifierValue(int amount, int time)
            {
                Amount = amount;
                Time = time;
            }
        }

        private List<StatModifierValue> m_Modifiers;

        public StatModifierController()
        {
            m_Modifiers = new List<StatModifierValue>();
        }

        public void AddModifer(int amount, int turns)
        {
            m_Modifiers.Add(new StatModifierValue(amount, turns));
        }
        
        public void UpdateController()
        {
            for (int i = m_Modifiers.Count - 1; i >= 0; i--)
            {
                m_Modifiers[i].Time--;
                if (m_Modifiers[i].Time <= 0)
                {
                    m_Modifiers.RemoveAt(i);
                }
            }
        }

        public int GetModifier()
        {
            var result = 0;
            foreach (var modifier in m_Modifiers)
            {
                result += modifier.Amount;
            }

            return result;
        }
    }
}