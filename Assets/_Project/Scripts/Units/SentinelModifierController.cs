﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kairos.Units
{
    public class SentinelModifierController
    {
        public class SentinelModifierValue
        {
            public int Amount;
            public int Timeframe;

            public SentinelModifierValue(int amount, int timeframe)
            {
                Amount = amount;
                Timeframe = timeframe;
            }
        }

        private List<SentinelModifierValue> m_Modifiers;

        public SentinelModifierController()
        {
            m_Modifiers = new List<SentinelModifierValue>();
        }

        public void AddModifier(int amount, int timeframe)
        {
            m_Modifiers.Add(new SentinelModifierValue(amount, timeframe));
        }

        public bool CheckModifier(int timeframe)
        {
            foreach(SentinelModifierValue modifierValue in m_Modifiers)
            {
                if(modifierValue.Timeframe == timeframe)
                {
                    return true;
                }
            }

            return false;
        }

        public int GetModifierAmount(int timeframe)
        {
            int totalValue = 0;

            foreach(SentinelModifierValue modifierValue in m_Modifiers)
            {
                if(modifierValue.Timeframe == timeframe)
                {
                    totalValue += modifierValue.Amount;
                }
            }

            return totalValue;
        }

        public void ClearModifiers()
        {
            m_Modifiers.Clear();
        }
    }
}

