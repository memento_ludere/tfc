﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Kairos.Units.Editor
{
    [CustomPropertyDrawer(typeof(UnitAttribute))]
    public class UnitAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var unitIds = UnitDatabase.GetIdList();

            var actualId = Array.IndexOf(unitIds, property.stringValue);
            if (actualId < 0)
                actualId = unitIds.Length;
            
            var unitNames = new List<string>();
            foreach (var unit in unitIds)
            {
                unitNames.Add(UnitDatabase.LoadedUnits[unit].UnitName);
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(label, GUILayout.ExpandWidth(false));
            var newId = EditorGUILayout.Popup(actualId, unitNames.ToArray());
            EditorGUILayout.EndHorizontal();

            if (newId != actualId)
                property.stringValue = newId < unitIds.Length ? unitIds[newId] : "";
        }
    }
}