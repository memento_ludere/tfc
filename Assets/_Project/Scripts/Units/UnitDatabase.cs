﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Kairos.Units
{
    public static class UnitDatabase
    {
        private static Dictionary<string, UnitInfo> m_LoadedUnits;

        public static Dictionary<string, UnitInfo> LoadedUnits
        {
            get
            {
                if (m_LoadedUnits == null)
                    Reload();
                return m_LoadedUnits;
            }
        }

        public static UnitInfo[] GetUnitsList()
        {
            UnitInfo[] result = new UnitInfo[LoadedUnits.Count];

            int counter = 0;
            foreach (var loadedUnit in LoadedUnits)
            {
                result[counter] = loadedUnit.Value;
                counter++;
            }

            return result;
        }

        public static string[] GetIdList()
        {
            string[] result = new string[LoadedUnits.Count];

            int counter = 0;
            foreach (var loadedUnit in LoadedUnits)
            {
                result[counter] = loadedUnit.Key;
                counter++;
            }

            return result;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        public static void Reload()
        {
            m_LoadedUnits = new Dictionary<string, UnitInfo>();

            var filesToLoad = Directory.GetFiles(Globals._UNITS_FOLDER);
            foreach (var file in filesToLoad)
            {
                if (Path.GetExtension(file).ToLower() == ".unit")
                {
                    var newUnit = UnitHelper.LoadUnit(file);
                    LoadedUnits.Add(newUnit.UnitID, newUnit);
                }
            }
        }
    }
}