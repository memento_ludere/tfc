﻿using System;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.Events;

namespace Kairos.Indicators
{
    public class CursorController : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UnityEvent m_OnIdle;
        [SerializeField] private UnityEvent m_OnAlly;
        [SerializeField] private UnityEvent m_OnEnemy;
    
        private Camera m_CursorCamera;

        private void Start()
        {
            m_CursorCamera = GameObject.FindWithTag("UICamera").GetComponent<Camera>();
            Cursor.visible = false;
            m_OnIdle?.Invoke();
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.TargetSelected, OnTargetSelected);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.TargetUnselected, OnTargetUnselected);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.TargetEnemySelected, OnTargetEnemySelected);
        }

        private void OnDestroy()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.TargetSelected, OnTargetSelected);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.TargetUnselected, OnTargetUnselected);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.TargetEnemySelected, OnTargetEnemySelected);
        }

        private void FixedUpdate()
        {
            transform.position = m_CursorCamera.ScreenToWorldPoint(Input.mousePosition) + m_CursorCamera.transform.forward;
        }
        
        private void Update()
        {
            transform.position = m_CursorCamera.ScreenToWorldPoint(Input.mousePosition) + m_CursorCamera.transform.forward;
        }
        
        private void LateUpdate()
        {
            transform.position = m_CursorCamera.ScreenToWorldPoint(Input.mousePosition) + m_CursorCamera.transform.forward;
        }

        private void OnTargetSelected(GenericEventArgs args, Action listenerExecutedCallback)
        {
            m_OnAlly?.Invoke();
            listenerExecutedCallback?.Invoke();
        }

        private void OnTargetUnselected(GenericEventArgs args, Action listenerExecutedCallback)
        {
            m_OnIdle?.Invoke();
            listenerExecutedCallback?.Invoke();
        }
        
        private void OnTargetEnemySelected(GenericEventArgs args, Action listenerExecutedCallback)
        {
            m_OnEnemy?.Invoke();
            listenerExecutedCallback?.Invoke();
        }
    }
}
