﻿using System;
using TorkFramework;
using TorkFramework.Events;

namespace Kairos.Indicators{
public class CursorTrigger : EntityBehaviour<GameEntity>
{
    private bool m_Selected = false;
    
    public void OnSelect()
    {
        m_Selected = true;
        Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TargetSelected, this);
    }

    public void OnDeselect()
    {
        m_Selected = false;
        Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TargetUnselected, this);
    }

    private void OnDisable()
    {
        if(m_Selected)
            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TargetUnselected, this);
    }
}
}