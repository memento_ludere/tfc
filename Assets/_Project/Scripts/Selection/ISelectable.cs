﻿using Kairos.Units;
using UnityEngine;

public interface ISelectable
{
    void OnSelect(Team selectionTeam);
    void OnDeselect();
}
