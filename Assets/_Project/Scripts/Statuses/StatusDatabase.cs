﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Statuses
{
    public static class StatusDatabase
    {
        [NonSerialized, XmlSerialized] private static SerializationHolder<List<StatusInfo>> m_Statuses;
        public static List<StatusInfo> Statuses => m_Statuses.SerializedObject;

        [RuntimeInitializeOnLoadMethod]
        private static void Init()
        {
            Load();
        }

        public static void Save()
        {
            var serializer = new Serializer(new XmlWriterSettings());
            if (m_Statuses == null)
                m_Statuses = new SerializationHolder<List<StatusInfo>> {SerializedObject = new List<StatusInfo>()};
            serializer.SerializeToFile(Globals._STATUS_DATABASE_FILE, m_Statuses);
        }

        public static void Load()
        {
            if (!File.Exists(Globals._STATUS_DATABASE_FILE))
                Save();
            var deserializer = new Deserializer();
            m_Statuses = deserializer.DeserializeFromFile<SerializationHolder<List<StatusInfo>>>(Globals._STATUS_DATABASE_FILE);
        }

        public static List<StatusInfo> GetUnitStatuses(Units.UnitController unitController)
        {
            List<StatusInfo> statusesInfo = new List<StatusInfo>();

            foreach(StatusInfo info in Statuses)
            {            
                if (info.ApplyCheck.CheckStatus(unitController))
                {
                    statusesInfo.Add(info);
                }
            }         
            return statusesInfo;
        }

    }
}
