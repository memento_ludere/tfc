﻿using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Statuses.Checks
{
    public class StrengthModifierStatusCheck : StatusCheck
    {
        [XmlSerialized] private ComparisionType m_Comparison;

        public override bool CheckStatus(UnitController targetUnit)
        {
            if (m_Comparison == ComparisionType.Greater)
                return targetUnit.StrengthModifier > 0;
            else
                return targetUnit.StrengthModifier < 0;
        }

        public override int GetStatusValue(UnitController targetUnit)
        {
            if (m_Comparison == ComparisionType.Greater)
                return targetUnit.StrengthModifier;
            else
                return -targetUnit.StrengthModifier;
        }
    }

}