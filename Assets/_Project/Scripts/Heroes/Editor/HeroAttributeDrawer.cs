﻿using System;
using System.Collections.Generic;
using Kairos.Units;
using UnityEditor;
using UnityEngine;

namespace Kairos.Heroes.Editor
{
    [CustomPropertyDrawer(typeof(HeroAttribute))]
    public class HeroAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var heroIds = HeroDatabase.GetIdList();

            var actualId = Array.IndexOf(heroIds, property.stringValue);
            if (actualId < 0)
                actualId = heroIds.Length;

            var heroNames = new List<string>();
            foreach (var hero in heroIds)
            {
                heroNames.Add(UnitDatabase.LoadedUnits[HeroDatabase.LoadedHeroes[hero].HeroUnit].UnitName);
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(label, GUILayout.ExpandWidth(false));
            var newId = EditorGUILayout.Popup(actualId, heroNames.ToArray());
            EditorGUILayout.EndHorizontal();
            
            if (newId != actualId)
            {
                property.stringValue = newId < heroIds.Length ? heroIds[newId] : "";
            }
        }
    }
}