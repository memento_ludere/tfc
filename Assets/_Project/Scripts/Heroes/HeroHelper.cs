﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using TorkFramework.Serialization;
using UnityEditor;

namespace Kairos.Heroes
{
    public static class HeroHelper
    {
        public static void SaveHero(HeroInfo hero, string path)
        {
#if UNITY_EDITOR
            if (!Directory.Exists(Path.GetDirectoryName(path)))
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            
            var serializer = new Serializer(new XmlWriterSettings());
            serializer.SerializeToFile(path, hero);

            AssetDatabase.Refresh();
#endif
        }
        
        public static HeroInfo LoadHero(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();

            if (Path.GetExtension(path).ToLower() != ".hero")
                throw new BadImageFormatException();

            var deserializer = new Deserializer();
            return deserializer.DeserializeFromFile<HeroInfo>(path);
        }
        
        private static string GenerateHeroId()
        {
            var result = default(byte[]);

            using (var stream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
                {
                    writer.Write(DateTime.Now.Ticks);
                }

                stream.Position = 0;

                using (var hash = SHA256.Create())
                {
                    result = hash.ComputeHash(stream);
                }
            }

            var resultText = "";
            for (int i = 0; i < result.Length; i++)
            {
                resultText += result[i].ToString("x2");
            }

            return "hero-" + resultText;    
        }
        
        public static HeroInfo GenerateNewHero()
        {
            return new HeroInfo {HeroId = GenerateHeroId()};
        }
    }
}