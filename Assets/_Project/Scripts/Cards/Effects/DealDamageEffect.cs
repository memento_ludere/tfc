﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;
using TorkFramework.VFX;
using UnityEngine;

namespace Kairos.Cards.Effects
{
    [CardGenerationPattern("deal <%i:DamageAmount> damages")]
    public class DealDamageEffect : CardEffect
    {
        [XmlSerialized] public int DamageAmount;

        public override void ExecuteEffect(CardInfo sourceCard, UnitController sourceUnit, UnitController[] targets, TimelineController timeline, Action effectExecutedCallback)
        {
            var remainingTargetsCount = targets?.Length ?? 0;
            if (remainingTargetsCount > 0)
            {
                for (int i = 0; i < targets.Length; i++)
                {
                    var targetIndex = i;
                    if (VFXPrefab?.Resource == null)
                    {
                        if (sourceUnit.CheckSentinelDamageBoost(timeline.ActualTimeframe))
                        {
                            targets[targetIndex].DealDamage(sourceUnit, DamageAmount + sourceUnit.GetSentinelDamageBoostAmount(timeline.ActualTimeframe), () =>
                            {
                                remainingTargetsCount--;
                                if (remainingTargetsCount <= 0)
                                {
                                    effectExecutedCallback?.Invoke();
                                }
                            });
                        }
                        else
                        {
                            targets[targetIndex].DealDamage(sourceUnit, DamageAmount, () =>
                            {
                                remainingTargetsCount--;
                                if (remainingTargetsCount <= 0)
                                {
                                    effectExecutedCallback?.Invoke();
                                }
                            });
                        }
                    }
                    else
                    {
                        sourceUnit.Entity.GameManager.GetManager<VFXManager>().PlayVFX(
                            (VFXPrefab.Resource as GameObject).GetComponent<VFXEntity>(),
                            EffectVFXs.ToList(),
                            sourceUnit.transform,
                            targets[targetIndex].transform,
                            () =>
                            {
                                if (sourceUnit.CheckSentinelDamageBoost(timeline.ActualTimeframe))
                                {
                                    targets[targetIndex].DealDamage(sourceUnit, DamageAmount + sourceUnit.GetSentinelDamageBoostAmount(timeline.ActualTimeframe), () =>
                                    {
                                        remainingTargetsCount--;
                                        if (remainingTargetsCount <= 0)
                                        {
                                            effectExecutedCallback?.Invoke();
                                        }
                                    });
                                }
                                else
                                {
                                    targets[targetIndex].DealDamage(sourceUnit, DamageAmount, () =>
                                    {
                                        remainingTargetsCount--;
                                        if (remainingTargetsCount <= 0)
                                        {
                                            effectExecutedCallback?.Invoke();
                                        }
                                    });
                                }
                            });
                    }
                }
            }
            else
            {
                effectExecutedCallback?.Invoke();
            }
        }

        public override void ExecuteEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, UnitController sourceUnit, UnitController[] targets, TimelineController timeline, BoardController board)
        {
            if (targets != null && targets.Length > 0)
            {
                if (targets[0].Team == Team.Player)
                {
                    if (sourceUnit.Team == Team.Player)
                        snapshot.HeroSnapshot.DealDamage(snapshot.HeroSnapshot, DamageAmount);
                    else
                        snapshot.HeroSnapshot.DealDamage(snapshot.EnemiesSnapshots[Array.IndexOf(board.EnemyUnits, sourceUnit)], DamageAmount);
                }
                else
                {
                    var indexes = new List<int>();
                    foreach (var target in targets)
                    {
                        indexes.Add(Array.IndexOf(board.EnemyUnits, target));
                    }

                    foreach (var index in indexes)
                    {
                        if (sourceUnit.Team == Team.Player)
                            snapshot.EnemiesSnapshots[index].DealDamage(snapshot.HeroSnapshot, DamageAmount);
                        else
                            snapshot.EnemiesSnapshots[index].DealDamage(snapshot.EnemiesSnapshots[Array.IndexOf(board.EnemyUnits, sourceUnit)], DamageAmount);
                    }
                }
            }
        }

        public override string GetEffectValue(TimelineController.TimelineUnitSnapshot? unitSnapshot)
        {
            if (unitSnapshot.HasValue)
            {
                var calculatedValue = Mathf.Clamp(DamageAmount + unitSnapshot.Value.StrengthModifier + unitSnapshot.Value.SentinelDamageBoost < 0 ? 0 : DamageAmount + unitSnapshot.Value.StrengthModifier+ unitSnapshot.Value.SentinelDamageBoost, 0, 999);
                var variation = calculatedValue - DamageAmount;
                if (variation > 0)
                    return $"<color=\"green\"> {DamageAmount} (+{variation})</color>";

                if (variation < 0)
                    return $"<color=\"red\"> {DamageAmount} ({variation})</color>";

                return DamageAmount.ToString();
            }

            return DamageAmount.ToString();
        }
    }
}