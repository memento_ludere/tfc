﻿using System;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Cards.Selection
{
    [CardGenerationPattern("to an <%t:TargetTeam>")]
    public class TargetSelectionFunction : CardSelectionFunction
    {
        [XmlSerialized] public Team TargetTeam;

        public override void GetTargets(CardInfo sourceCard, UnitController sourceUnit, SelectionController selection, BoardController board,TimelineController timeline, int timeframe, Action<bool, UnitController[]> targetsAcquiredCallback)
        {
            var layerMask = TargetTeam == Team.Player ? LayerMask.GetMask("Hero") : LayerMask.GetMask("Enemy");
            selection.SelectObject(sourceUnit.transform.position, layerMask, result =>
            {
                if (result == null)
                {
                    targetsAcquiredCallback?.Invoke(false, new UnitController[0]);
                }
                else
                {
                    var controller = board.GetUnit(result);
                    if (controller)
                    {
                        if (controller.Team == TargetTeam)
                        {
                            targetsAcquiredCallback?.Invoke(true, new[] {controller});
                        }
                        else
                        {
                            targetsAcquiredCallback?.Invoke(false, new UnitController[0]);
                        }
                    }
                    else
                    {
                        targetsAcquiredCallback?.Invoke(false, new UnitController[0]);
                    }
                }
            });
        }
    }
}