﻿using System;
using System.Collections.Generic;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Cards.Selection
{
    [CardGenerationPattern("to all <%t:TargetTeam>")]
    public class TeamSelectionFunction : CardSelectionFunction
    {
        [XmlSerialized] public Team TargetTeam;
        
        public override void GetTargets(CardInfo sourceCard, UnitController sourceUnit, SelectionController selection, BoardController board,TimelineController timeline, int timeframe, Action<bool, UnitController[]> targetsAcquiredCallback)
        {
            List<UnitController> result = new List<UnitController>();
            if (TargetTeam == Team.Player)
            {
                result.Add(board.HeroUnit);
            }
            else
            {
                for (int i = 0; i < board.EnemyUnits.Length; i++)
                {
                    if(board.EnemyUnits[i] != null && !board.EnemyUnits[i].IsDead)
                        result.Add(board.EnemyUnits[i]);
                }
            }
            
            targetsAcquiredCallback?.Invoke(true, result.ToArray());
        }
    }
}