﻿using System;
using TorkFramework.Serialization;

namespace Kairos.Cards
{
    [Serializable]
    public class CardReference
    {
        [XmlSerialized] public string Id;
        public CardInfo Card => CardDatabase.LoadedCards[Id];

        public CardReference()
        {
            Id = "";
        }
    }
}