﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Cards.EffectSelectors
{
    [CardGenerationPattern("repeat <%i:RepeatAmount>")]
    public class RepeatEffectSelector : CardEffectSelector
    {
        [XmlSerialized] public int RepeatAmount;
        public override bool CheckEffect(TimelineController.TimelineAction action, UnitController source, UnitController[] targets, TimelineController timeline, BoardController board, out bool overrideEffects, out int repetitions)
        {
            overrideEffects = false;
            repetitions = RepeatAmount;
            return true;
        }

        public override bool CheckEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, out bool overrideEffects, out int repetitions)
        {
            overrideEffects = false;
            repetitions = RepeatAmount;
            return true;
        }

        public override string GetSelectorValue(ref TimelineController.TimelineUnitSnapshot? snapsho)
        {
            return RepeatAmount.ToString();
        }
    }
}