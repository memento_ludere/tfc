﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Cards.EffectSelectors
{
    [CardGenerationPattern("stagger <%i:TargetDamage>")]
    public class StaggerEffectSelector : CardEffectSelector
    {
        [XmlSerialized] public int TargetDamage;
        public override bool CheckEffect(TimelineController.TimelineAction action, UnitController source, UnitController[] targets, TimelineController timeline, BoardController board, out bool overrideEffects, out int repetitions)
        {
            repetitions = 1;
            overrideEffects = false;
            if (source.TurnTakedDamage >= TargetDamage)
                return false;
            return true;
        }

        public override bool CheckEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, out bool overrideEffects, out int repetitions)
        {
            repetitions = 1;
            overrideEffects = false;
            if (action.Source == snapshot.HeroSnapshot.SourceUnit)
            {
                if (snapshot.HeroSnapshot.TurnTakedDamage >= TargetDamage)
                    return false;
            }
            else
            {
                foreach (var enemiesSnapshot in snapshot.EnemiesSnapshots)
                {
                    if (enemiesSnapshot.SourceUnit == action.Source)
                    {
                        if (enemiesSnapshot.TurnTakedDamage >= TargetDamage)
                            return false;
                    }
                }
            }
           
            return true;
        }

        public override string GetSelectorValue(ref TimelineController.TimelineUnitSnapshot? snapshot)
        {
            if (snapshot.HasValue)
                return snapshot.Value.TurnTakedDamage + "/" + TargetDamage;
            return TargetDamage.ToString();
        }
    }
}