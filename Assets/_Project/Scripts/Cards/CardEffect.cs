﻿using System;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;
using TorkFramework.VFX.Animations;

namespace Kairos.Cards
{
    public abstract class CardEffect
    {
        [XmlSerialized] public ResourceReference VFXPrefab;
        [XmlSerialized] public VFXAnimation[] EffectVFXs;
        public abstract void ExecuteEffect(CardInfo sourceCard, UnitController sourceUnit, UnitController[] targets, TimelineController timeline, Action effectExecutedCallback);
        public abstract void ExecuteEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, UnitController sourceUnit, UnitController[] targets, TimelineController timeline, BoardController board);
        public abstract string GetEffectValue(TimelineController.TimelineUnitSnapshot? unitSnapshot);
    }
}