﻿using System;
using System.Collections.Generic;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Cards
{
    
    public class CardSentence
    {
        [XmlSerialized] public List<CardSelectionFunction> Selections;
        [XmlSerialized] public List<CardEffectSelector> EffectSelectors;
        [XmlSerialized] public List<CardEffect> Effects;

        public CardSentence()
        {
            Selections = new List<CardSelectionFunction>();
            Effects = new List<CardEffect>();
            EffectSelectors = new List<CardEffectSelector>();
        }

        public void GetTargets(CardInfo sourceCard, UnitController sourceUnit, SelectionController selection, BoardController board, TimelineController timeline, int timeframe, Action<bool, UnitController[]> targetsAcquiredCallback)
        {
            GetTargetsRecursive(0, new List<UnitController>(), sourceCard, sourceUnit, selection, board, timeline, timeframe, targetsAcquiredCallback);
        }

        private void GetTargetsRecursive(int actualIndex, List<UnitController> targets, CardInfo sourceCard, UnitController sourceUnit, SelectionController selection, BoardController board, TimelineController timeline, int timeframe, Action<bool, UnitController[]> targetsAcquiredCallback)
        {
            if (actualIndex >= Selections.Count)
            {
                targetsAcquiredCallback?.Invoke(true, targets.ToArray());
            }
            else
            {
                Selections[actualIndex].GetTargets(sourceCard, sourceUnit, selection, board,timeline, timeframe, (result, newTargets) =>
                {
                    if (!result)
                    {
                        targetsAcquiredCallback?.Invoke(false, null);
                    }
                    else
                    {
                        if (newTargets != null)
                        {
                            foreach (UnitController newTarget in newTargets)
                            {
                                if (!targets.Contains(newTarget))
                                    targets.Add(newTarget);
                            }
                        }

                        GetTargetsRecursive(actualIndex+ 1, targets, sourceCard, sourceUnit, selection, board,timeline, timeframe, targetsAcquiredCallback);
                    }
                });
            }
        }

        public bool CheckPlayable(TimelineController.TimelineAction action,UnitController sourceUnit,UnitController[] targets,TimelineController timeline, BoardController board,out bool overrideSenteces, out int selectedRepetitions)
        {
            overrideSenteces = false;
            selectedRepetitions = 1;
            foreach (var selector in EffectSelectors)
            {
                if (selector.CheckEffect(action, sourceUnit, targets, timeline, board, out var selectorOverrideEffects, out var repetitions))
                {
                    overrideSenteces = overrideSenteces || selectorOverrideEffects;
                    selectedRepetitions = repetitions > selectedRepetitions ? repetitions : selectedRepetitions;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public bool CheckPlayableOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, out bool overrideSenteces, out int selectedRepetitions)
        {
            overrideSenteces = false;
            selectedRepetitions = 1;
            foreach (var selector in EffectSelectors)
            {
                if (selector.CheckEffectOnSnapshot(ref snapshot, action, timeline, out var selectorOverrideEffects, out var repetitions))
                {
                    overrideSenteces = overrideSenteces || selectorOverrideEffects;
                    selectedRepetitions = repetitions > selectedRepetitions ? repetitions : selectedRepetitions;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public void ExecuteEffect(CardInfo sourceCard, UnitController sourceUnit, UnitController[] targets, TimelineController timeline, BoardController board, Action effectExecutedCallback)
        {
            if (Effects.Count <= 0)
            {
                effectExecutedCallback?.Invoke();
            }
            else
            {
                ExecuteSingleEffect(sourceCard, 0, sourceUnit, targets, timeline, effectExecutedCallback);
            }
        }

        public void ExecuteEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot,  UnitController sourceUnit, UnitController[] targets, TimelineController timeline, BoardController board)
        {
            foreach (var effect in Effects)
            {
                effect.ExecuteEffectOnSnapshot(ref snapshot, sourceUnit, targets, timeline, board);
            }
        }

        private void ExecuteSingleEffect(CardInfo sourceCard, int effectIndex, UnitController sourceUnit, UnitController[] targets, TimelineController timeline, Action effectExecutedCallback)
        {
            Effects[effectIndex].ExecuteEffect(sourceCard, sourceUnit, targets, timeline, () =>
                {
                    effectIndex++;
                    if(effectIndex >= Effects.Count)
                        effectExecutedCallback?.Invoke();
                    else
                        ExecuteSingleEffect(sourceCard, effectIndex,sourceUnit,targets,timeline, effectExecutedCallback);
                }
            );
        }
    }
}