﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Kairos.Cards
{
    public static class CardDatabase
    {
        private static Dictionary<string, CardInfo> m_LoadedCards;
        public static Dictionary<string, CardInfo> LoadedCards
        {
            get
            {
                if(m_LoadedCards == null)
                    Reload();
                return m_LoadedCards;
            }
        }
        
        public static CardInfo[] GetCardList()
        {
            CardInfo[] result = new CardInfo[LoadedCards.Count];

            int counter = 0;
            foreach (var loadedCard in m_LoadedCards)
            {
                result[counter] = loadedCard.Value;
                counter++;
            }
        
            return result;
        }
        public static string[] GetIdList()
        {
            string[] result = new string[LoadedCards.Count];

            int counter = 0;
            foreach (var loadedCard in m_LoadedCards)
            {
                result[counter] = loadedCard.Key;
                counter++;
            }
        
            return result;
        }
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        public static void Reload()
        {
            m_LoadedCards = new Dictionary<string, CardInfo>();

            var filesToLoad = Directory.GetFiles(Globals._CARDS_FOLDER);
            foreach (var file in filesToLoad)
            {
                if (Path.GetExtension(file).ToLower() == ".card")
                {
                    var newCard = CardHelper.LoadCard(file);
                    LoadedCards.Add(newCard.CardId, newCard);
                }
            }
        }
    }
}