﻿using System;
using System.Collections;
using System.Collections.Generic;
using TorkFramework.Editor.Utility;
using TorkFramework.ResourceManagement;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;

namespace Kairos.Sentinels.Editor
{
    [CustomEditor(typeof(SentinelObject))]
    public class EncounterGroupObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var targetObject = (SentinelObject)target;

            targetObject.Sentinel.SentinelName = (string)FieldUtils.AdaptiveField("Name", targetObject.Sentinel.SentinelName);
            targetObject.Sentinel.SentinelDescription = (string)FieldUtils.AdaptiveField("Description", targetObject.Sentinel.SentinelDescription);
            targetObject.Sentinel.SentinelIcon = (ResourceReference)FieldUtils.AdaptiveField("Icon", targetObject.Sentinel.SentinelIcon);
            targetObject.Sentinel.MaxActivations = (int)FieldUtils.AdaptiveField("Max Activations", targetObject.Sentinel.MaxActivations);
            targetObject.Sentinel.Effect = (ResourceReference)FieldUtils.AdaptiveField("Effect", targetObject.Sentinel.Effect);


            GUI.color = Color.green;
            if (GUILayout.Button("Save"))
            {
                targetObject.Save();
                EditorUtility.SetDirty(targetObject);
                AssetDatabase.SaveAssets();
            }
        }
    }
}
