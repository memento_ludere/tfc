﻿using System;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Sentinels
{
	[XmlSerialized]
	public class SentinelInfo
	{
		[XmlSerialized] public string SentinelName;
		[XmlSerialized] public string SentinelDescription;
		[XmlSerialized] public ResourceReference SentinelIcon;
		[XmlSerialized] public ResourceReference SentinelActionToken;
		[XmlSerialized] public ResourceReference Effect;
		[XmlSerialized] public int MaxActivations;

		public SentinelInfo()
		{
			this.SentinelName = "SENTINEL NAME";
			this.SentinelDescription = "SENTINEL DESCRIPTION";
			this.SentinelIcon = null;
			this.Effect = null;
			this.MaxActivations = 1;
		}

		public SentinelInfo(SentinelInfo source)
		{
			this.SentinelName = source.SentinelName;
			this.SentinelDescription = source.SentinelDescription;
			this.SentinelIcon = source.SentinelIcon;
			this.Effect = source.Effect;
			this.MaxActivations = source.MaxActivations;
		}
	}
}

