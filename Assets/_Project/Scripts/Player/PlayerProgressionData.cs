﻿using System.Collections.Generic;
using System.Linq;
using Kairos.Heroes;
using TorkFramework.Serialization;

namespace Kairos.Player
{
    public class PlayerProgressionData
    {
        [XmlSerialized] private List<string> m_UnlockedHeroes;

        public List<string> UnlockedHeroes => m_UnlockedHeroes;

        public PlayerProgressionData()
        {
            m_UnlockedHeroes = new List<string>();
        }
        
        public PlayerProgressionData(List<string> unlockedHeroes)
        {
            m_UnlockedHeroes = unlockedHeroes;
        }

        public void UnlockHero(string hero ){
            if (HeroDatabase.LoadedHeroes.ContainsKey(hero) && !UnlockedHeroes.Contains(hero))
            {
                UnlockedHeroes.Add(hero);
            }
        }
    }
}