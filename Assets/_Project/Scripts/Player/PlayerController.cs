﻿using System;
using System.Collections.Generic;
using Kairos.Cards;
using Kairos.Controllers;
using Kairos.Sentinels;
using Kairos.Timeline;
using Kairos.UI.Player;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using TorkFramework.Utility;
using UnityEngine;

namespace Kairos.Player
{
    public class PlayerController : EntityBehaviour<GameEntity>
    {
        public enum PlayerStates
        {
            Idle,
            PlayingCard,
            WaitingTimeline,
            SelectingSentinel
        }
        
        private BoardController m_Board;
        private TimelineController m_Timeline;

        private List<CardInfo> m_DeckPile;
        private List<CardInfo> m_DiscardPile;
        private List<CardInfo> m_ExhaustPile;
        private List<CardInfo> m_Hand;

        private UnitController m_Hero;
        private List<SentinelController> m_Sentinels = new List<SentinelController>();
        private UIPlayerController m_UIController;

        private PlayerStates m_ActualState = PlayerStates.Idle;

        public List<CardInfo> DeckPile => m_DeckPile;
        public List<CardInfo> DiscardPile => m_DiscardPile;
        public List<CardInfo> ExhaustPile => m_ExhaustPile;
        public List<CardInfo> Hand => m_Hand;
        public TimelineController Timeline => m_Timeline;

        public PlayerStates ActualState
        {
            get => m_ActualState;
            set => m_ActualState = value;
        }

        private PlayerCommandStack m_CommandStack;
        [SerializeField] private GameEntity m_SentinelControllerPrefab;
        
        public void Load(BoardController board, TimelineController timeline, PlayerData playerData, Action controllerLoadedCallback)
        {
            m_UIController = Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIPlayerController)] as UIPlayerController;
            m_UIController.HandView.Player = this;
            m_Board = board;
            m_Timeline = timeline;
            m_CommandStack = new PlayerCommandStack(Entity.GameManager, this);
            
            m_Board.SpawnHero(playerData.Hero, (spawnedHero) =>
            {
                m_Hero = spawnedHero;
                SpawnSentinels(playerData.Sentinels, () =>
                {
                    m_DeckPile = new List<CardInfo>();
                    m_DiscardPile = new List<CardInfo>();
                    m_ExhaustPile = new List<CardInfo>();
                    m_Hand = new List<CardInfo>();

                    foreach (var card in playerData.Cards)
                    {
                        m_DeckPile.Add(new CardInfo(CardDatabase.LoadedCards[card]));
                    }
                    
                    ShuffleDeck();
                    
                    controllerLoadedCallback?.Invoke();
                });
            });
        }

        public void Unload()
        {
        }

        private void SpawnSentinels(List<SentinelInfo> sentinels,Action sentinelsSpawnedCallback)
        {
            m_UIController.ClearView();
            for (int i = 0; i < sentinels.Count; i++)
            {
                SentinelController newSentinel = Entity.GameManager.Spawn(m_SentinelControllerPrefab).GetComponent<SentinelController>();
                newSentinel.SentinelData = sentinels[i];
                newSentinel.TimelineController = m_Timeline;
                newSentinel.InitializeSentinel();
                m_Sentinels.Add(newSentinel);
                m_UIController.SpawnSentinelButtons(newSentinel);
            }
            sentinelsSpawnedCallback?.Invoke();
            return;
        }

        private void ShuffleDeck()
        {
            m_DeckPile = m_DeckPile.Shuffle();
        }

        public void DrawCards(int cardsToDraw, Action cardsDrawCallback)
        {
            if (cardsToDraw <= 0)
            {
                cardsDrawCallback?.Invoke();
                return;
            }

            if (m_DeckPile.Count <= 0)
            {
                if (m_DiscardPile.Count <= 0)
                    throw new Exception("Deck and Discards are empty");

                while (m_DiscardPile.Count > 0)
                {
                    m_DeckPile.Add(m_DiscardPile[0]);
                    m_DiscardPile.RemoveAt(0);
                }

                ShuffleDeck();
            }

            var targetCard = m_DeckPile[0];
            
            m_Hand.Add(targetCard);
            m_DeckPile.Remove(targetCard);
            
            m_UIController.DrawCard(targetCard, m_Board.HeroUnit, () =>
            {
                DrawCards(cardsToDraw - 1, cardsDrawCallback);
            });
        }

        private void DiscardCards(int cardsToDiscard, bool animate, Action cardsDiscardedCallback)
        {
            if (cardsToDiscard <= 0)
            {
                cardsDiscardedCallback?.Invoke();
                return;
            }

            if (m_Hand.Count <= 0)
            {
                cardsDiscardedCallback?.Invoke();
                return;
            }

            DiscardCard(m_Hand[0], animate, () => DiscardCards(cardsToDiscard - 1, animate, cardsDiscardedCallback));
        }

        public void DiscardCard(CardInfo card, bool animate, Action cardDiscardedCallback)
        {
            if (!m_Hand.Contains(card))
            {
                cardDiscardedCallback?.Invoke();
                return;
            }
            
            m_DiscardPile.Add(card);
            m_Hand.Remove(card);
            
            m_UIController.DiscardCard(card, animate, cardDiscardedCallback);
        }
        
        public void ResetCard(CardInfo card, Action cardResettedCallback){
            if (m_DiscardPile.Contains(card))
            {
                m_DiscardPile.Remove(card);
                m_Hand.Add(card);
                
                m_UIController.DrawCard(card, m_Hero, () =>
                {
                    cardResettedCallback?.Invoke();
                });
            }
            else
            {
                cardResettedCallback?.Invoke();
            }
        }

        private void ExhaustCard(CardInfo card, bool animate, Action cardExhaustedCallback)
        {
            if (!m_Hand.Contains(card))
            {
                cardExhaustedCallback?.Invoke();
                return;
            }
            
            m_ExhaustPile.Add(card);
            m_Hand.Remove(card);

            m_UIController.ExhaustCard(card, animate, cardExhaustedCallback);
        }

        public void OnTurnStarted(Action turnPreparedCallback)
        {
            DrawCards(Globals.START_TURN_HAND_SIZE, turnPreparedCallback);
        }

        public void OnTurnEnd(Action turnEndedCallback)
        {
            m_CommandStack.Flush();
            DiscardCards(m_Hand.Count, true, turnEndedCallback);
        }

        public void PlayTimeline()
        {
            m_ActualState = PlayerStates.WaitingTimeline;
            m_UIController.ControllerState = UIPlayerController.UIPlayerControllerState.WaitingTimeline;
            for(int i = 0; i <= Globals.TIMELINE_SIZE;i++)
            {
                m_Timeline.ExecuteSentinelActionsInTimeframe(i,() =>
                {
                    if(i == Globals.TIMELINE_SIZE)
                    {
                        m_Timeline.ExecuteTimeline(() => 
                        { 
                            m_ActualState = PlayerStates.Idle;
                            m_UIController.ControllerState = UIPlayerController.UIPlayerControllerState.Idle;
                            m_Timeline.m_SentinelActionsExecuted = true;
                        });
                    }
                });
            }
        }

        private void OnSentinelPlacedRequest(GenericEventArgs args, Action listenerEventCallback)
        {
            if(m_UIController.ControllerState == UIPlayerController.UIPlayerControllerState.PlacingSentinel)
            { 
                Debug.Log("PLACED");
                PlaySentinelCommand command = new PlaySentinelCommand(m_Timeline, (SentinelController)args.Args[0], (int)args.Args[1]);
                m_CommandStack.Do(command, result => listenerEventCallback?.Invoke());
            }
            else
            {
                Debug.Log("WAT");
                listenerEventCallback?.Invoke();
            }
        }

        private void OnEnable()
        {
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.StartTimelineRequest, OnStartTimelineRequest);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.PlayCardRequest, OnPlayCardRequest);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.UndoRequest, OnUndoRequest);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.SentinelPlaced, OnSentinelPlacedRequest);
        }

        private void OnDisable()
        { 
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.StartTimelineRequest, OnStartTimelineRequest);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.PlayCardRequest, OnPlayCardRequest);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.UndoRequest, OnUndoRequest);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.SentinelPlaced, OnSentinelPlacedRequest);
        }

        private void OnStartTimelineRequest(GenericEventArgs args, Action listenerExecutedCallback)
        {
            listenerExecutedCallback?.Invoke();
            if (m_ActualState == PlayerStates.Idle && m_UIController.ControllerState == UIPlayerController.UIPlayerControllerState.Idle)
            {
                PlayTimeline();
            }
        }

        private void OnPlayCardRequest(GenericEventArgs args, Action listenerExecutedCallback)
        {
            if (m_ActualState == PlayerStates.Idle && m_UIController.ControllerState == UIPlayerController.UIPlayerControllerState.Idle)
            {
                var card = args.Args[0] as CardInfo;
                if (card != null && Hand.Contains(card))
                {
                    var command = new PlayCardCommand(m_Board, m_Timeline, card);
                    m_CommandStack.Do(command, result =>
                    {
                        m_Timeline.UpdateSnapshots();
                        m_UIController.HandView.UpdateCardUnitSnapshots();
                        listenerExecutedCallback?.Invoke();
                    });
                }
                else
                {
                    listenerExecutedCallback?.Invoke();
                }
            }
            else
            {
                listenerExecutedCallback?.Invoke();
            }
        }

        public void OnUndoRequest(GenericEventArgs args, Action listenerExecutedCallback)
        {
            if(m_ActualState == PlayerStates.Idle)
                m_CommandStack.Undo(result =>
                {
                    m_Timeline.UpdateSnapshots();
                    m_UIController.HandView.UpdateCardUnitSnapshots();
                    listenerExecutedCallback?.Invoke();
                });
            else
                listenerExecutedCallback?.Invoke();
        }
    }
}