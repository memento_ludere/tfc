﻿using System.Collections;
using System.Collections.Generic;
using Kairos.Heroes;
using Kairos.Sentinels;
using Kairos.Units;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Player
{
    [SerializeField]
    public class PlayerData
    {
        [XmlSerialized] public UnitData Hero;
        [XmlSerialized] public List<SentinelInfo> Sentinels;
        [XmlSerialized] public List<string> Cards;
        [XmlSerialized] public int PotionCount;
        [XmlSerialized] public string HeroId;

        public PlayerData()
        {
            Hero = new UnitData();
            Sentinels = new List<SentinelInfo>();
            Cards = new List<string>();
        }

        public PlayerData(string hero, List<SentinelInfo> sentinels)
        {
            HeroId = hero;
            var heroInfo = HeroDatabase.LoadedHeroes[hero];
            Hero = new UnitData(UnitDatabase.LoadedUnits[heroInfo.HeroUnit]);
            Sentinels = sentinels;
            PotionCount = heroInfo.PotionCount;

            Cards = new List<string>();
            foreach (var card in heroInfo.Cards)
            {
                Cards.Add(card);
            }
        }

        public void AddCard(string card)
        {
            Cards.Add(card);
        }

        public void RemoveCard(string card)
        {
            if (Cards.Contains(card))
                Cards.Remove(card);
        }

        public void AddSentinel(SentinelInfo sentinel)
        {
            if (Sentinels.Count < Globals.MAX_SENTINELS)
                Sentinels.Add(sentinel);
        }

        public void RemoveSentinel(SentinelInfo sentinel)
        {
            if (Sentinels.Contains(sentinel))
                Sentinels.Remove(sentinel);
        }
    }
}
