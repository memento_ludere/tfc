﻿using Kairos.Sentinels;
using Kairos.Timeline;
using System;
using System.Collections;
using System.Collections.Generic;
using TorkFramework;
using UnityEngine;

namespace Kairos.Player
{
    public class PlaySentinelCommand : PlayerCommand
    {
        private TimelineController.SentinelAction m_Action;
        private bool m_Executed = false;
        private TimelineController Timeline;
        private SentinelController Source;
        private int TargetTimeframe;

        public PlaySentinelCommand(TimelineController timeline, SentinelController source, int targetTimeframe)
        {
            Timeline = timeline;
            Source = source;
            TargetTimeframe = targetTimeframe;
        }

        public override void Execute(GameManager gameManager, PlayerController player, Action<bool> commandExecuteCallback)
        {
            Source.TimelineController.AddSentinelAction(Source, TargetTimeframe, () => commandExecuteCallback?.Invoke(true)); ;
        }

        public override void Undo(GameManager gameManager, PlayerController player, Action<bool> commandUndoCallback)
        { 
            Source.UndoSentinelActionFromTimeline(Source, () => commandUndoCallback?.Invoke(true));
        }
    }
}


