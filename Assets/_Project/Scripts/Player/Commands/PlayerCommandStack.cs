﻿using System;
using System.Collections.Generic;
using TorkFramework;

namespace Kairos.Player
{
    public class PlayerCommandStack
    {
        private readonly Stack<PlayerCommand> m_CommandStack;
        private readonly GameManager m_GameManager;
        private readonly PlayerController m_Player;
    
        public PlayerCommandStack(GameManager gameManager, PlayerController player)
        {
            m_CommandStack = new Stack<PlayerCommand>();
            m_GameManager = gameManager;
            m_Player = player;
        }

        public void Flush()
        {
            m_CommandStack.Clear();
        }

        public void Do(PlayerCommand command, Action<bool> doDoneCallback)
        {
            if (command == null)
            {
                doDoneCallback(false);
                return;
            }

            command.Execute(m_GameManager, m_Player, result =>
            {
                if (result)
                    m_CommandStack.Push(command);
                doDoneCallback?.Invoke(result);
            });
        }

        public void Undo(Action<bool> doDoneCallback)
        {
            if (m_CommandStack.Count <= 0)
            {
                doDoneCallback?.Invoke(false);
                return;
            }

            m_CommandStack.Pop().Undo(m_GameManager, m_Player, doDoneCallback);
        }
    }
}