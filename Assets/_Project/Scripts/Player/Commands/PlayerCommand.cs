﻿using System;
using TorkFramework;

namespace Kairos.Player
{
    public abstract class PlayerCommand
    {
        protected object[] m_Parameters;

        protected PlayerCommand(params object[] parameters)
        {
            m_Parameters = parameters;
        }
        
        public abstract void Execute(GameManager gameManager, PlayerController player, Action<bool> commandExecuteCallback);
        public abstract void Undo(GameManager gameManager, PlayerController player, Action<bool> commandUndoCallback);
    }
}
